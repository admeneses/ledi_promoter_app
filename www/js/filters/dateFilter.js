(function(){
	'use strict';

	angular
		.module('starter')
		.filter('date', date)

	//função para filtrar a exibição da data
	function date(){
		return function(input){
			var newInput = '';

			//verifica se está passando nulo
			if(input === ' ' || !input){
				newInput = input;
			}else{
				var year, month, day;

				year = input.substring(0, 4);
				month = input.substring(5, 7);
				day = input.substring(8, 10);

				newInput = day + "/" + month;
			}

			return newInput;
		}
	}
})();