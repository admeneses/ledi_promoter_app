(function(){
	'use strict';

	angular
		.module('starter')
		.filter('time', time)

	//função para filtrar a exibição da data e hora
	function time(){
		return function(input){
			var newInput = '';

			//verifica se está passando nulo
			if(input === ' ' || !input){
				newInput = '-- --';
			}else{
                var hour, minutes;
                
				hour = input.substring(11, 13);
				minutes = input.substring(14, 16);

				newInput = hour + ":" + minutes;
			}

			return newInput;
		}
	}
})();