(function(){
	"use strict";

	angular
		.module("starter")
		.constant("QUESTIONS", [
      //atribuindo as perguntas do Check List
			{
        		'type': 'radio',
        		'options': ['Sim', 'Não'],
        		'title': '1. Foi validado os promotores dentro dos horários, roupas e depois o cartão?'
      		},
      		{
        		'type': 'select',
            'options': ['1', '2','3','4','5','6','7','8','9','10'],
        		'title': '2. Como a loja se encontra?',
            'subtitle' : '(ruim-01, bom-05 ou excelente-10)'
      		},
      		{
        		'type': 'select',
            'options': ['1', '2','3','4','5','6','7','8','9','10'],
        		'title': '3. Como estão os itens de  promoção, arrumação, precificação e quantidades?',
            'subtitle' : '(ruim-01, bom-05 ou excelente-10)'
      		},
      		{
        		'type': 'radio',
        		'options': ['Sim', 'Não'],
        		'title': '4. O pedido da loja está sendo feito ou acompanhado ou dando ideias pelo líder da Benassi?'
      		},
      		{
        		'type': 'select',
            'options': ['1', '2','3','4','5','6','7','8','9','10'],
        		'title': '5. Como estão o comportamento e atitudes dos promotores da Benassi na loja?',
            'subtitle' : '(ruim-01, bom-05 ou excelente-10)'
      		},
      		{
        		'type': 'radio',
        		'options': ['Sim', 'Não'],
        		'title': '6. Foi passado algum ensinamento para promotores?',
        		'obs': 'Qual?'
      		},
      		{
        		'type': 'select',
            'options': ['1', '2','3','4','5','6','7','8','9','10'],
        		'title': '7. Qual o grau de satisfação com gerente da loja - com nossos serviços?',
            'subtitle' : '(ruim-01, bom-05 ou excelente-10)'
      		},
      		{
        		'type': 'radio',
        		'options': ['Sim', 'Não'],
        		'title': '8. O cartão de ponto está em dia?'
      		},
      		{
        		'type': 'radio',
            'options': ['Sim', 'Não'],
        		'title': '9. As horas extras feitas pela loja estão anotadas no cartão de ponto? '
      		},
      		{
        		'type': 'select',
            'options': ['1', '2','3','4','5','6','7','8','9','10'],
        		'title': '10. Está satisfatório a chegada do caminhão da Benassi na loja (ponto de vista Gerencia e RM)?',
            'subtitle' : '(ruim-01, bom-05 ou excelente-10)'
      		},
      		{
        		'type': 'radio',
        		'options': ['Sim', 'Não'],
        		'title': '11. Essa loja pertence a sua rota diária?',
        		'obs2': 'Motivo: '
      		}
		])
})();