(function() {
	'use strict';
	
	// criação daas tabelas no SQLite
	angular
		.module('starter')
		.constant('SQLITE_CONFIG',{
			'name' : 'dbLediPromoter',
			'location' : 'default',
			'tables' : {
				'tbFuncionarios' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'INTEGER',
					'nome' : 'TEXT',
					'sobrenome' : 'TEXT',
					'email' : 'TEXT',
					'username' : 'TEXT',
					'senha' : 'TEXT',
					'cargo' : 'TEXT',
					'status' : 'TEXT',
					'idChecklist' : 'INTEGER',
					'lojaFunc' : 'TEXT',
					'lojaLat' : 'TEXT',
					'lojaLong' : 'TEXT'
				},
				'tbMarketing' : {
					'id' : 'INTEGER', 
					'mensagem' : 'TEXT',
					'created_at' : 'TEXT'
				},
				'tbRoutes' : {
					'id' : 'INTEGER',
					'nome' : 'TEXT',
					'funcionario' : 'INTEGER',
					'dia_da_semana' : 'INTEGER',
					'data': 'TEXT'
				},
				'tbCartao_ponto' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'TEXT',
					'checkin' : 'TEXT',
					'checkout' : 'TEXT',
					'cartao_pontocol' : 'TEXT',
					'latitude' : 'TEXT',
					'longitude' : 'TEXT',
					'status' : 'TEXT'
				},
				'tbLogin' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'username' : 'TEXT', 
					'password' : 'TEXT', 
					'dataHora' : 'TEXT', 
					'status' : 'TEXT', 
					'versao' : 'TEXT',
					'uuid' : 'TEXT',
					'model' : 'TEXT',
					'versionDevice' : 'TEXT'
				},
				'tbDebug' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'mensagem' : 'TEXT',
					'erro' : 'TEXT',
					'dataHora' : 'TEXT'
				},
				'tbMarcacao' : {
					'id' : 'INTEGER',
					'status': 'TEXT'
				},
				'tbConfiguracao' : {
					'id' : 'INTEGER',
					'gpsAtivo' : 'INTEGER',
					'metrosAtivo' : 'INTEGER',
					'cargoQuinhentos' : 'INTEGER',
					'lojasInseridas' : 'INTEGER'
				},
				'tbLoja' : {
					'id' : 'INTEGER',
					'codigo' : 'INTEGER',
					'nome' : 'TEXT',
					'latitude' : 'TEXT',
					'longitude' : 'TEXT'
				},
				'tbPermissao' : {
					'funcionario' : 'INTEGER',
					'id' : 'TEXT',
					'tipo' : 'TEXT',
					'modulo' : 'TEXT'
				},
				'tbAvisoBloqueio' : {
					'id' : 'INTEGER',
					'bloqueiahora' : 'INTEGER',
					'bloqueiagpsmock' : 'INTEGER'
				},
				'tbCartao_ponto2' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'TEXT',
					'checkin' : 'TEXT',
					'checkout' : 'TEXT',
					'cartao_pontocol' : 'TEXT',
					'latitude' : 'TEXT',
					'longitude' : 'TEXT',
					'status' : 'TEXT',
					'notacoes' : 'TEXT'
				},
				'tbLojas' : {
					'id' : 'INTEGER',
					'codigo' : 'INTEGER',
					'nome' : 'TEXT'
				},
				'tbCheckListDinamico' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'idChecklist' : 'INTEGER',
					'funcionario' : 'INTEGER',
					'checklist': 'TEXT'
				},
				'tbCheckListDinamicoResposta' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'INTEGER',
					'checklist': 'TEXT',
					'dataHora': 'TEXT',
					'loja': 'INTEGER'
				}
			}
		}) 
})();
