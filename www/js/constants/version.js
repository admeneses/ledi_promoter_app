(function () {
    'use strict';

    angular
        .module('starter')
        .constant('VERSION', {
            name: '',
            value: '1.9.6',
            date: '06/09/2018'
        })
})();