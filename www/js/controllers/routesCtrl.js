angular
	.module('starter')
	.controller('routesCtrl', routesCtrl)
	routesCtrl.$inject = ['$scope', 'Network', 'WebService', 'Check', 'DB'];

    function routesCtrl($scope, Network, WebService, Check, DB){
        var vm = this;
		
		// exibir div de carregamento
		$scope.load = false;

		var week =  ['Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sábado'];

		var date = new Date();
        date = calculateWeekBeginEnd(date.getFullYear(), date.getMonth() + 1, date.getDate());
		var days = getDaysInTheWeek(date.begin, date.end);

        // date = calculateWeekBeginEnd('2017', '2','28');
        console.log(date);

		vm.next = next;
	    vm.previous = previous;
		vm.prepareRoutes = prepareRoutes;
		vm.size = week.length - 1;
	    vm.title = '';
		vm.count = new Date().getDay();
		vm.todayRoutes = null;
		vm.routes = null;
		vm.day = null;
		
		var init = true;

		if(init){
			// iniciar recebimento das rotas
			initRoutes();
			init = false;
		}
	
		// iniciar dia da semana
		initQuestion();
	    function initQuestion(){
	        changeTitle();
	    }

		// altera o dia da semana
	    function changeTitle(){
	        vm.title = week[vm.count];
	        vm.day = days[vm.count];
	    }

		// altera as rotas do dia
		function changeTodayRoutes(){
			vm.todayRoutes = vm.routes[vm.count];
		}

		// avançar dia
	    function next(question){
			if(vm.count >= 0 && vm.count < vm.size){
				vm.count++;
	        	changeTitle();
				changeTodayRoutes();
			}else{
				//do nothing
			}
	    }

		// voltar dia
	    function previous(){
			if(vm.count > 0){
				vm.count--;
				changeTitle();
				changeTodayRoutes();
			}else{
				//do nothing
			}
	    }

		// separando as rotas por dia da semana
		function prepareRoutes(values){
			//Apagando dados das rotas
			DB.dropRoutes().then(function(data){
				console.log(data);
			}, function(err){
				console.log(err);
			});

			var routes = {};
			//algoritmo de recomendação
			for(var i = 0; i < values.length; i++){
				routes[values[i].dia_da_semana] = routes[values[i].dia_da_semana] || [];
				routes[values[i].dia_da_semana].push(values[i]);

				//Inserir no banco de dados
				DB.saveRoutes(values[i].id, values[i].nome, values[i].funcionario, values[i].dia_da_semana, values[i].data).then(function(data){
		            console.log(data);
		       	}, function(err){
		       		console.log(err);
		       	});
			}

			//todas as rotas (semanal)
			vm.routes = routes;
			//console.log(routes);
			
			//dia atual
			vm.todayRoutes = routes[new Date().getDay()];
		}

		// filtrando semana
		// começo e final 
		function calculateWeekBeginEnd(year, month, date) {
            var newMonth    = month, 
                newYear     = year,
                totalMonth  = new Date(year, month, 0).getDate(), 
                today       = new Date(year, month - 1, date),
                day         = today.getDay(),
                firstDay    = (today.getDate() - day),
                lastDay     = (today.getDate() + (6 - day));
            
            if((today.getDate() + 6) > totalMonth && (6 - today.getDay()) + today.getDate() > totalMonth) {
                var lastDayInTheMonth = new Date(year, month - 1, totalMonth).getDay();
                
                lastDay = 0 + 6 - lastDayInTheMonth;
                
                if(month + 1 > 12) {
                    newYear = year + 1;
                    newMonth = 1;
                } else {
                    newMonth++;
                }
            }
            
            if((today.getDate() - 6) < 0 && (1 - day) <= 0) {
                var pastMonth = new Date(year, month - 1, 0).getDate();
                firstDay = pastMonth + (today.getDate() - day);
                if(month - 1 < 1) {
                    year = year - 1;
                    month = 12;
                } else {
                    month--;
                }
            }

			// begin = year +"-"+ ((month < 10) ? '0'+month:month) +'-'+ ((firstDay < 10) ? '0'+firstDay:firstDay);
            // end = newYear +"-"+ ((newMonth < 10) ? '0'+newMonth:newMonth) +'-'+ ((lastDay < 10) ? '0'+lastDay:lastDay);
			
			return {
            	begin: year +"-"+ ((month < 10) ? '0'+month:month) +'-'+ ((firstDay < 10) ? '0'+firstDay:firstDay),
            	end: newYear +"-"+ ((newMonth < 10) ? '0'+newMonth:newMonth) +'-'+ ((lastDay < 10) ? '0'+lastDay:lastDay)
            }
        }

		//dias da semana
		function getDaysInTheWeek(begin, end) {
			var days     = [],
				beginDay = begin.split('-')[2] * 1,
				endDay   = end.split('-')[2] * 1,
				diff     = (endDay - beginDay) + 1,
				max      = 1000,
				count    = 0;

			if(endDay < beginDay) {
				diff = 7; max = (7 - endDay) - 1;
			}

			for(var i = 0; i < diff; i++) {
				days[i] = beginDay + count;

				if(max == 0) {
					beginDay = 0; count = 0; max = 100;
				}
				else max--;

				count++;
			}

			return days;
		}

		// convertendo o dia da semana
		// function convertDays(year, month, days) {
		// 	var newDays = []

		// 	for(var i = 0; i < days.length; i++) {
		// 		newDays[i] = [year, month, days[i]]; 
		// 	}

		// 	return days;
		// }

		function initRoutes(){
			//conexão com webservice routes.php
			if(Network.status == true){
				$scope.load = true;
				WebService.routes(Check.id, date.begin, date.end).success(function(data) {
					//console.log(data);
					$scope.load = false;				   	
					if(data.result.geral == 'Sucesso') {
						var values = data.values;
						// console.log(2);

						// função para separar as rotas
						vm.prepareRoutes(data.values.geral); 
					}else {
						console.log(data);
					}	            
				});
			} else if(Network.status == false){
				var routes = [];
				DB.routes().then(function(data){
					console.log(data.rows.length);
					if(data.rows.length){
						for(var i = 0; i < data.rows.length; i++){
							var route = data.rows.item(i);
							routes[route.dia_da_semana] = routes[route.dia_da_semana] || [];
							routes[route.dia_da_semana].push(route);
						}

						vm.routes = routes;
						// dia atual
						vm.todayRoutes = routes[new Date().getDay()];
					} else{
						//do nothing
					}
				}, function(err){
					console.log(err);
				})
			}else{
				//nothing
			}
		}

    }