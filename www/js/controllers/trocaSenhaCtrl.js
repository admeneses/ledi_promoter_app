angular
.module('starter')
.controller('trocaSenhaCtrl', trocaSenhaCtrl)
trocaSenhaCtrl.$inject = ['$scope', 'Network', 'Popup', 'DateHour', 'WebService', 'Check', 'DB'];

function trocaSenhaCtrl($scope, Network, Popup, DateHour, WebService, Check, DB){
    $scope.send = false;

    $scope.solicitar = function(username, password, newPassword, repeatPassword){        
        if(Network.status){
            if(username === Check.username && repeatPassword === newPassword && password === Check.senha){
                $scope.send = true;

                WebService.trocaSenha(Check.id, newPassword).success(function(data) {
                    $scope.send = false;
                    console.log(data);
                    if(data.result == 'Sucesso'	) {
                        DB.updatePassword(Check.id, newPassword).then(function(data){
                            console.log('senha alterada');
                            Check.senha = newPassword;
                            Popup.alteracaoConcluida();
                        }, function(err){
                            console.log(err);
                            Popup.alteracaoFalha();
                        });
                    }else{
                        Popup.alteracaoFalha();
                    }
                });
            }else if(username !== Check.username){
                Popup.alteracaoUsuarioInvalida();
            }else if(password !== Check.senha){
                Popup.alteracaoSenhaInvalida();
            }else if(repeatPassword !== newPassword){
                Popup.alteracaoNovaSenhaInvalida();
            }else{
                Popup.alteracaoFalha();
            }
        }else{
            Popup.alteracaoOffline(); 
        }
    }
}