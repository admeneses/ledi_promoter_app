(function(){
    'use strict';

    angular
        .module('starter')
        .controller('imagesCtrl', imagesCtrl)
        imagesCtrl.$inject = ['$scope', 'Popup', 'Network', '$cordovaCamera', 'Check', 'DateHour', 'WebService', '$cordovaFileTransfer'];

        function imagesCtrl($scope, Popup, Network, $cordovaCamera, Check, DateHour, WebService, $cordovaFileTransfer){
 
            $scope.send = false;
            $scope.pictureURL = '';
            var format = 'JPEG';
            var selected = 0;
            $scope.checkeds = [];
            $scope.pictures = [];

            var pictureSource;   // picture source
            var destinationType; // sets the format of returned value

            $scope.selectPictureToRemove = function(pos, isChecked){
                isChecked = !isChecked;
                if(isChecked){
                    $scope.checkeds.push(pos);
                }else{
                    $scope.checkeds.splice($scope.checkeds.indexOf(pos), 1);
                }
                $scope.pictures[pos].checked = isChecked;
                // console.log($scope.checkeds);
            }

            $scope.removePicture = function(){
                $scope.checkeds.sort();
                for(var i = $scope.checkeds.length-1; i >= 0; i--){
                    $scope.pictures.splice($scope.checkeds[i], 1);
                }
                $scope.checkeds = [];
                $scope.pictureURL = '';
                // console.log($scope.checkeds);
            }

            document.addEventListener("deviceready", onDeviceReady, false);
            
            function onDeviceReady() {
                pictureSource = navigator.camera.PictureSourceType;
                destinationType = navigator.camera.DestinationType;
            }

            function clearCache() {
                if($scope.count == $scope.pictures.length-1){
                    $scope.send = false;
                    $scope.pictures = [];
                    $scope.pictureURL = '';
                    Popup.sendingImage();
                }else{
                    $scope.count++;
                }
                // console.log($scope.count, $scope.pictures.length);
                navigator.camera.cleanup();
            }
            
            $scope.onCapturePhoto = function() {
                if(Network.status){
                    $scope.send = true;
                    $scope.count = 0;
                    var i, fileURI, selected;
                    for(i = 0; i < $scope.pictures.length; i++){
                        fileURI = $scope.pictures[i].name;
                        selected = $scope.pictures[i].selected;

                        $scope.sendImage(fileURI, selected, 0); 
                    }
                }else{
                    Popup.imagesFail();
                }                
            }
           
            $scope.sendImage = function(fileURI, selected, retries){                       
                var win = function (r) {
                    clearCache();
                    retries = 0;
                    // alert('Done!');
                    // console.log(fileURI);
                }

                var fail = function (error) {
                    // console.log(error, fileURI);
                    if (retries == 0) {
                        retries++
                        setTimeout(function() {
                            $scope.sendImage(fileURI, selected, retries)
                        }, 1000)
                    } else {
                        clearCache();
                        // alert('Ups. Something wrong happens!');
                    }
                }
                
                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
                options.mimeType = "image/jpeg";
                //console.log(options.fileName);

                if(selected === 1){
                    options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1) + ".jpg";
                    var str = options.fileName;
                    var res = str.substr(str.lastIndexOf("?") + 1, str.length);
                    options.fileName = res;
                    //console.log(options.fileName);
                }

                options.params = {}; // if we need to send parameters to the server request
                options.params.funcionario = Check.id;
                options.params.cartao_pontocol = Check.pontocol || Check.pontocol2;
                options.params.format = format;
                options.params.dataHora = DateHour.checkImage();
                options.params.tipo = 1;       
                
                //console.log(options.params);
                var ft = new FileTransfer();

                ft.upload(fileURI, encodeURI("http://promoter.ledicloud.com/test/webservices/upload_415.php"), win, fail, options);
                
                //'http://promoter.ledicloud.com/webservices/upload_415.php'
                //'http://192.168.0.24:80/Projetos_LEDi/LEDi_PROMOTER/webservices/';
            }

            $scope.capturePhoto = function() {
                navigator.camera.getPicture(onSuccess, onFail, {
                    quality: 100,
                    destinationType: destinationType.FILE_URI,
                    // allowEdit: true,
                    targetWidth: 1280,
                    targetHeight: 800,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation:true
                });
            }

            $scope.selectPhoto = function() {
                navigator.camera.getPicture(onSuccess, onFail, {
                    quality: 100,
                    destinationType: destinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    targetWidth: 1280,
                    targetHeight: 800,
                    popoverOptions: CameraPopoverOptions,
                    correctOrientation:true
                });
                selected = 1;
            }

            function onFail(message) {
                // alert('Failed because: ' + message);
                console.log(message);
            }

            function onSuccess(fileURI){
                $scope.pictureURL = fileURI;
                Popup.imageCaptured();
                // console.log(selected);
                $scope.pictures.push({"name": fileURI, "selected": selected, "checked": false});
                selected = 0;
                //console.log($scope.pictures);
            }   
        }
})();

