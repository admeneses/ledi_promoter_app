angular
	.module('starter')
	.controller('sobreCtrl', sobreCtrl)
	sobreCtrl.$inject = ['$scope', '$stateParams', '$state', 'VERSION', 'User', 'DB', 'Popup', 'Localizacao'];

	function sobreCtrl($scope, $stateParams, $state, VERSION, User, DB, Popup, Localizacao){
		//exibindo versão na tela
		$scope.versao = VERSION.name + ' ' + VERSION.value; 
		$scope.data = VERSION.date;

		//ativar bootão debug
		$scope.debugging = 0;

		// função para verificar para qual tela o botão "voltar" deve realizar
		// tela de Login ou Controle de Presença
		$scope.redirectBack = function(){
			$state.go($stateParams.previousState);
		}

		//verificar o parametro no contato.html
		$scope.next = $stateParams.previousState;

		$scope.dados = function(){
			User.dados();
		}
		
		$scope.dadosSQLite = function(){
			User.dadosSQLite();
		}

		$scope.reset = function(){
			User.removeAll(function (){
				alert('Registros resetados!');
			});
		}

		$scope.checkDebug = function(){
			$scope.debugging = 1;
			Popup.modeDebugOn();
		}
		
		$scope.debug = function(){
			User.debug();
		}

		$scope.deleteDebug = function(){
			DB.deleteDebug().then(function(data){
				alert('Informações de erros apagadas!');
			}, function(err){
				console.log(err);
			});
		}

		$scope.latitude = Localizacao.lat;
		$scope.longitude = Localizacao.longt;
	}