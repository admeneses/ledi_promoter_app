angular
	.module('starter')
	.controller('espelhoColaboradorCtrl', espelhoColaboradorCtrl)
	espelhoColaboradorCtrl.$inject = ['$scope', '$stateParams', 'WebService', 'Network', 'Popup', 'Espelho', 'Check'];

	function espelhoColaboradorCtrl($scope, $stateParams, WebService, Network, Popup, Espelho, Check){
        //desativando div "carregando"
        $scope.load = false;

        //definindo orientação da tela
        screen.orientation.lock('landscape').then(function success() {
            console.log("Successfully locked the orientation");
        }, function error(errMsg) {
            console.log("Error locking the orientation :: " + errMsg);
        });

        // função para receber as entradas e saídas da rotina do colaborador
		$scope.buscarRotina = function(begin, end){           
            // console.log(begin);
            // console.log(end);

            var newBegin = begin.getFullYear() + '-' + (begin.getMonth() + 1) + '-' + begin.getDate();
            // console.log(newBegin);

            var newEnd = end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate();
            // console.log(newEnd);

            var auxEnd = begin.getFullYear() + '-' + ((begin.getMonth() + 1) + 1) + '-' + begin.getDate();
            // console.log(auxEnd);

            if(Network.status){
                // ativando div "carregando"
                $scope.load = true;
                if(moment(newEnd).isAfter(newBegin)){
                    if(moment(newEnd).isAfter(auxEnd)){
                        $scope.load = false;
                        Popup.espelhoMensal();
                    }else{
                        $scope.result = [];
                        WebService.espelhoColaborador(Check.id, newBegin, newEnd, true).success(function(data) {
                            //desativando div "carregando"
                            $scope.load = false;
                            // console.log(data);
                            // console.log('array', data.cartao_ponto);
                            var order = Espelho.formatTable(data.cartao_ponto);
                            // console.log('order', order);
                            $scope.result = Espelho.arrangeRoutine(order[0], data.info.loja, data.info.timeToLunch);
                            console.log($scope.result);

                            if($scope.result.length == 0){
                                return Popup.espelhoFail();
                            }  
                        });
                    }
                }else{
                    $scope.load = false;
                    Popup.espelhoValido();
                }
			}else{
				Popup.espelhoOffline();
			}
        }
	}