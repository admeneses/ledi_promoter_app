angular
	.module('starter')
	.controller('loginCtrl', loginCtrl)
	loginCtrl.$inject = ['$cordovaDevice', '$scope', '$state', 'Network', 'DB', 'Check', '$window', 'Localizacao', 'WebService','Popup', 'DateHour', 'User', 'Message', 'VERSION'];

	function loginCtrl($cordovaDevice, $scope, $state, Network, DB, Check, $window, Localizacao, WebService, Popup, DateHour, User, Message, VERSION){

		// function getPermissoes(funcionario){
		// 	$scope.load = true;

		// 	WebService.receberPermissoes(Check.id).success(function(res) {
		// 		$scope.load = false;
		// 		// console.log(res);

		// 		var values = res;

		// 		// console.log(values);

		// 		// console.log('values', values);

		// 		DB.deletePermissao(Check.id).then(function(data){
		// 			console.log(data);
		// 			for(var i = 0; i < values.length; i++){
		// 				// console.log(values[i].id, values[i].type, values[i].code);
		// 				DB.insertPermissao(Check.id, values[i].id, values[i].type, values[i].code).then(function(data){
		// 					console.log(data);
		// 				}, function(err){
		// 					console.log(err);
		// 				});
		// 			}
		// 		}, function(err){
		// 			console.log(err);
		// 		});
		// 	})
		// }
		
		var timeoutUser = null;
		function initUsername() {
			timeoutUser = setTimeout(function(){
				DB.userSave().then(function(data){
					// console.log(data);
					if(data.rows.length){
						// for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(0);
							$scope.username = values.username;
							$scope.password = values.senha;
							// alert(values.username);
						// }
					}else{
						$scope.username = '';
						$scope.password = '';
					}
				}).catch( function(err) {
					// console.log(err);
					clearTimeout(timeoutUser); // Limpa a requisição passada
					initUsername();
				})
			}, 2000)
		}

		// setInterval(Localizacao.GPS(), 10000);		
		// Localizacao.posicao();
		
		initUsername();

		//informações do dispositivo
		document.addEventListener("deviceready", function () {
			// var device = $cordovaDevice.getDevice();

			// var cordova = $cordovaDevice.getCordova();

			var model = $cordovaDevice.getModel();

			var platform = $cordovaDevice.getPlatform();

			var uuid = $cordovaDevice.getUUID();

			var version = $cordovaDevice.getVersion();

			$scope.model 		 = model;
			$scope.uuid  		 = uuid;
			$scope.versionDevice = platform + ' - ' + version;
		}, false);

		
		$scope.login = {};

		//localStorage.clear();
		Check.clearUserInfo();
		Check.clearStore();
		Check.clearCheck();
		Check.clearCheckList();
		DateHour.clearDate();
		Message.clearMessages();

		// exibir div de conexão
		$scope.load = false;
		
		//exibindo versão na tela
		$scope.versao = VERSION.name + ' ' + VERSION.value;

		var desatualizado = '';
		
		// função para efetuar Login
		//$scope.sendLogin = function (username, password){
		function sendLogin(username, password){
			var currentTimeLogin = DateHour.checkLogin();
			var modeLogin = '';

			if(Network.status == true){
				Localizacao.posicao();
				//modeLogin = 'online';
				var status = 'Online';
				
				$scope.load = true;
				// console.log(username, password, currentTimeLogin, status, VERSION.value, $scope.uuid, $scope.model, $scope.versionDevice);
				// chamando o service (connectionWebService) para fazer a autenticação do usuário
				WebService.doLogin(username, password, currentTimeLogin, status, VERSION.value, $scope.uuid, $scope.model, $scope.versionDevice).success(function(data) {
		            $scope.load = false;
		            console.log(data);
					desatualizado = data.result.tbVersao;

					if(!data.result.uuidValid){
						// return alert('Dispositivo não autorizado!');
						return Popup.dispositivoNaoAutorizado();
					}

					if(data.result.tbConfigMetros) {
						var values = data.values;  
						Check.metrosAtivo = values.tbConfigMetros.definido;
						// console.log(Check.metrosAtivo, 'metrosAtivo');
					}

					if(data.result.tbConfigGPS){
						var values = data.values; 
						Check.gpsAtivo = values.tbConfigGPS.definido;
						// console.log(Check.gpsAtivo, 'gpsAtivo');
					}

					if(data.result.tbBloqueiahora) {
						var values = data.values;  
						Check.bloqueiahora = values.tbBloqueiahora.definido;
						// console.log(Check.bloqueiahora, 'bloqueiahora');
					}

					if(data.result.tbBloqueiagpsmock){
						var values = data.values; 
						Check.bloqueiagpsmock = values.tbBloqueiagpsmock.definido;
						// console.log(Check.bloqueiagpsmock, 'bloqueiagpsmock');
					}

					if(data.result.tbfuncionario) {
						window.FirebasePlugin.unregister();
						var values = data.values;         	
						// grava os dados no Check Service
						Check.statusFunc = values.tbfuncionario.status;
						Check.id = values.tbfuncionario.id;
						Check.nome = values.tbfuncionario.nome;
						Check.sobrenome = values.tbfuncionario.sobrenome;
						Check.cargoAll = values.tbfuncionario.idCargo + " " + values.tbfuncionario.cargo;
						Check.email = values.tbfuncionario.email;
						Check.idChecklist = values.tbfuncionario.idChecklistLayout;
						Check.lojaFunc = values.tbfuncionario.lojaCodigo + " " + values.tbfuncionario.lojaFunc;
						Check.lojaLat = values.tbfuncionario.lojaLat;
						Check.lojaLong = values.tbfuncionario.lojaLong;
						Check.cargoQuinhentos = values.tbfuncionario.cargoQuinhentos;
						Check.username = username;
						Check.senha = password;

						// console.log(Check.cargoAll);
						
						Check.idCargo = Check.cargoAll.split(" ", 1);
						Check.cargo = Check.cargoAll.split(/ (.+)/)[1];

						Check.idCargo = Check.idCargo.toString();
						Check.cargo = Check.cargo.toString();

						// console.log(Check.idCargo);
						// console.log(Check.cargo);

						// console.log(Check.cargoQuinhentos, values.tbfuncionario.cargoQuinhentos);
						
						// document.addEventListener("deviceready", function(){
						// 	// Get notified when a token is refreshed
						// 	window.FirebasePlugin.onTokenRefresh(function(token) {
						// 		// save this server-side and use it to push notifications to this device
						// 		// console.log("Refresh to get new token: " + token);
						// 		Check.token = token;

						// 		console.log(Check.token);

						// 		WebService.sendToken(Check.id, Check.token).success(function(data) {
						// 			console.log(data);
						// 		});
		
						// 	}, function(error) {
						// 		alert(error);
						// 	});
					  
							// Get notified when the user opens a notification
						// 	window.FirebasePlugin.onNotificationOpen(function(notification) {
						// 		// console.log(JSON.stringify(notification));
						// 		console.log(notification);
						// 		Message.request();
						// 	}, function(error) {
						// 		console.error(error);
						// 	});  
						// }, false);

						WebService.receberLojas().success(function(data) {
							// console.log(data, data.length);
							if(data.length){
								//apagando do BD SQLite
								DB.deleteLojas().then(function(res){
									// console.log('Lojas deletadas');
									for(var i = 0; i < data.length; i++){
										// console.log(data[i]);
										//salvando no BD SQLite
										DB.saveLojas(data[i].id, data[i].nome, data[i].codigo).then(function(data){
											// console.log('Lojas salvas');
										}, function(err){
											console.log(err);
										});
									}
								}, function(err){
									console.log(err);
								});
							}
						});

						//salvando no BD SQLite
						DB.checkUser(values.tbfuncionario.id, values.tbfuncionario.nome, values.tbfuncionario.sobrenome, values.tbfuncionario.email, username, password, Check.cargoAll, values.tbfuncionario.status, values.tbfuncionario.idChecklistLayout, Check.lojaFunc, values.tbfuncionario.lojaLat, values.tbfuncionario.lojaLong).then(function(data){
							// console.log(data);
							DB.updateUser(values.tbfuncionario.id, values.tbfuncionario.nome, values.tbfuncionario.sobrenome, values.tbfuncionario.email, username, password, Check.cargoAll, values.tbfuncionario.status, values.tbfuncionario.idChecklistLayout, Check.lojaFunc, values.tbfuncionario.lojaLat, values.tbfuncionario.lojaLong).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}, function(err){
							console.log(err);
						});

						//salvando no BD SQLite
						DB.checkConfiguracao(1, Check.gpsAtivo, Check.metrosAtivo, Check.cargoQuinhentos, 0).then(function(data){
							// console.log('Configuração salva');
						}, function(err){
							console.log(err);
						});

						//salvando no BD SQLite
						DB.checkAvisoBloqueio(1, Check.bloqueiahora, Check.bloqueiagpsmock).then(function(data){
							// console.log('Configuração salva');
						}, function(err){
							console.log(err);
						});

						//recebendo permissões
						// getPermissoes();

						if(Check.statusFunc == 'INA'){
							return Popup.inativoFunc();
						}else{
							// buscando mensagens no banco de dados
							// Message.getLocalMessages();

							// redireciona para o sistema
							$window.location.href = '#/presenca';
							Popup.welcome();
							
							// DB.userSave().then(function(data){
							// 	if(data.rows.length){
							// 		for(var i = 0; i < data.rows.length; i++){
							// 			var values = data.rows.item(i);
							// 			// $scope.usuarioSalvo = values.username;
							// 			alert(values.username);
							// 		}
							// 	}else{
							// 		alert('FAIL');
							// 	}
							// }, function(err){
							// 	console.log(err);
							// });

							if(desatualizado == 'Desatualizado'){
								// $window.location.href = '#/versao';
								Popup.versao();
							}
						}
					}else if(!data.result.tbfuncionario){
						// console.log(data.result.tbfuncionario);
						// exibe um alert informando que o username e/ou senha estão inválidos
						Popup.usuarioInvalido();
					}else{
						var mensagem = 'Erro ao salvar Login Online';
						var dataHora = currentTimeLogin;
						var erro = data.result.tbfuncionario;
						console.log(mensagem, erro, dataHora);

						DB.saveDebug(mensagem, erro, dataHora).then(function(data){
							console.log('Debug Login Online salvo!');
						}, function(err){
							console.log('Erro ao salvar Login Online!', err);
						});
					}        
				});
			}else if(Network.status == false){
				//modeLogin = 'offline';
				var status = 'Offline';
				$scope.load = true;
				//alert('Você não possui conexão com internet!');
				DB.login().then(function(data){
					if(data.rows.length){
						//console.log(data.rows);
						var logado = 0;
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
							// console.log(values);
							if(username === values.username && password === values.senha){
								Check.id = values.funcionario;
				                Check.nome = values.nome;
				                Check.sobrenome = values.sobrenome;
				                Check.cargo = values.cargo;
				                Check.email = values.email;
								Check.statusFunc = values.status;
								Check.idChecklist = values.idChecklist;
								Check.lojaFunc = values.lojaFunc;
								Check.lojaLat = values.lojaLat;
								Check.lojaLong = values.lojaLong;

								Check.username = values.username;
								Check.senha = values.senha;
								
								// console.log(Check.cargo);
						
								Check.idCargo = Check.cargo.split(" ", 1);
								Check.cargo = Check.cargo.split(/ (.+)/)[1];
		
								Check.idCargo = Check.idCargo.toString();
								Check.cargo = Check.cargo.toString();
		
								// console.log(Check.idCargo);
								// console.log(Check.cargo);

								if(values.status == 'INA'){
									logado = 2;
								}else{
									DB.selectConfiguracao().then(function(data){
										if(data.rows.length){
											//console.log(data.rows);
											for(var i = 0; i < data.rows.length; i++){
												var values = data.rows.item(i);
												Check.gpsAtivo = values.gpsAtivo;
												Check.metrosAtivo = values.metrosAtivo;
												Check.cargoQuinhentos = values.cargoQuinhentos;
												// console.log(Check.cargoQuinhentos, values.cargoQuinhentos);
											}
										}else{
											//do nothing
										}
									}, function(err){
										console.log(err);
									});

									DB.selectAvisoBloqueio().then(function(data){
										if(data.rows.length){
											//console.log(data.rows);
											for(var i = 0; i < data.rows.length; i++){
												var values = data.rows.item(i);
												Check.bloqueiahora = values.bloqueiahora;
												Check.bloqueiagpsmock = values.bloqueiagpsmock;
												// console.log(Check.bloqueiahora, Check.bloqueiagpsmock);
											}
										}else{
											//do nothing
										}
									}, function(err){
										console.log(err);
									});

									//salvando os dados de Login Offline no SQLite
									var usernameOffline = values.username;
									var passwordOffline = values.senha;
									var versao = VERSION.value;

									// console.log(usernameOffline, currentTimeLogin, status, versao);

									DB.saveLogin(usernameOffline, passwordOffline, currentTimeLogin, status, versao, $scope.uuid, $scope.model, $scope.versionDevice).then(function(data){
										//desativando div "enviando"
										$scope.load = false;
										console.log('Login Salvo');
										$window.location.href = '#/presenca';
										Popup.avisos();
										Popup.welcome();

										// console.log(Check.metrosAtivo);
										// console.log(Check.gpsAtivo);
										// console.log(Check.lojaFunc, Check.lojaLat, Check.lojaLong);
										
									}, function(err){
										//desativando div "enviando"
										$scope.load = false;
										console.log(err);
										Popup.loginFail();

										var mensagem = 'Erro ao salvar Login Offline';
										var dataHora = currentTimeLogin;
										console.log(mensagem, err, dataHora);

										DB.saveDebug(mensagem, err, dataHora).then(function(data){
											console.log('Debug Check Out Offline salvo!');
										}, function(err){
											console.log('Erro ao salvar Debug Check Out Offline!', err);
										});
									});

									//salvando os dados no arquivo.json
									// var info = {
									//  "username": values.username,
									// 	"password": values.senha,
									// 	"currentTimeLogin": currentTimeLogin,
									// 	"status" : status,
									// 	"versao" : VERSION.value
									// }

									// User.save(info, function (data, err) {
									//     $scope.load = false;
									// 	// console.log(data, err);
									// 	if(err){
									// 		console.log(err);
									// 		Popup.loginFail();
									// 	}else{
									// 		$window.location.href = '#/presenca';
									// 		Popup.avisos();
									// 		Popup.welcome();
									// 	}	
									// })

									logado = 1;
									break;
								}
							}
						}
						
						if(logado == 0){
							$scope.load = false;
							Popup.usuarioInvalido();
						}else if(logado == 2){
							$scope.load = false;
							Popup.inativoFunc();
						}
					}else{
						$scope.load = false;
						alert('Nenhum usuário cadastrado no banco! (Modo offline)');
					}
				}, function(err){
					console.log(err);
				});
			}else{
				//  Popup.usuarioInvalido();
			}
			//$window.location.href = '#/presenca';
		}

		$scope.digital = function(username, password){
			// Check if device supports fingerprint
			/**
			* @return {
			*      isAvailable:boolean,
			*      isHardwareDetected:boolean,
			*      hasEnrolledFingerprints:boolean
			*   }
			*/
			// FingerprintAuth.isAvailable(function (result) {

			// 	console.log("FingerprintAuth available: " + JSON.stringify(result));
				
			// 	// If has fingerprint device and has fingerprints registered
			// 	if (result.isAvailable == true && result.hasEnrolledFingerprints == true) {
			// 		// Check the docs to know more about the encryptConfig object :)
			// 		var encryptConfig = {
			// 			clientId: "myAppName",
			// 			username: "currentUser",
			// 			password: "currentUserPassword",
			// 			maxAttempts: 5,
			// 			locale: "en_US",
			// 			dialogTitle: "Leitura de digital",
			// 			dialogMessage: "Coloque seu dedo no leitor do dispositivo",
			// 			dialogHint: "Ninguém roubará sua identidade"
			// 		}; // See config object for required parameters

			// 		// sendLogin(username, password);

			// 		// Set config and success callback
			// 		FingerprintAuth.encrypt(encryptConfig, function(_fingerResult){
			// 			console.log("successCallback(): " + JSON.stringify(_fingerResult));
			// 			if (_fingerResult.withFingerprint) {
			// 				console.log("Successfully encrypted credentials.");
			// 				console.log("Encrypted credentials: " + result.token);
			// 				sendLogin(username, password);  
			// 			} else if (_fingerResult.withBackup) {
			// 				console.log("Authenticated with backup password");
			// 				sendLogin(username, password);
			// 			}
			// 		// Error callback
			// 		}, function(err){
			// 				if (err === "Cancelled") {
			// 				console.log("FingerprintAuth Dialog Cancelled!");
			// 			} else {
			// 				console.log("Erro: " + err);
			// 			}
			// 		});
			// 	}else if (result.isAvailable == false){
					sendLogin(username, password);
			// 	}

			// /**
			// * @return {
			// *      isAvailable:boolean,
			// *      isHardwareDetected:boolean,
			// *      hasEnrolledFingerprints:boolean
			// *   }
			// */
			// }, function (message) {
			// 	console.log("isAvailableError(): " + message);
			// });
		}
	}