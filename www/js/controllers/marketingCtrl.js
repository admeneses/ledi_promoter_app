angular
	.module('starter')
	.controller('marketingCtrl', marketingCtrl)
	marketingCtrl.$inject = ['$scope', '$state', 'Message'];

	function marketingCtrl($scope, $state, Message){
		//apagando número de avisos				
		Message.clearMessages();

		//exibindo mensagens na tela de marketing
		$scope.mensagens = Message.listMessages;

		//exibindo mensagem de que não existem avisos
		$scope.noMessages = Message.noMessages;
		// Message.contAvisos = window.FirebasePlugin.setBadgeNumber(0);

		//abrindo link em uma webview
		$scope.openLink = function (link) {
		   window.open(link, '_self', 'location=yes');
		   return false;
		}
	}