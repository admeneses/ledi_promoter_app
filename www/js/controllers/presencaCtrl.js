angular
	.module('starter')
	.controller('presencaCtrl', presencaCtrl)
	presencaCtrl.$inject = ['$scope', '$state', 'Network', 'DB','$stateParams','$ionicSideMenuDelegate', 'DateHour', 'Check', 'Popup', 'Localizacao', 'WebService', 'User', 'Message', '$ionicPopup', 'Order', 'FileSystem'];

	function presencaCtrl($scope, $state, Network, DB, $stateParams, $ionicSideMenuDelegate, DateHour, Check, Popup, Localizacao, WebService, User, Message, $ionicPopup, Order, FileSystem){
		//definindo orientação da tela
		screen.orientation.lock('portrait').then(function success() {
			// console.log("Successfully locked the orientation");
		}, function error(errMsg) {
			console.log("Error locking the orientation :: " + errMsg);
		});

		function getPermissoes(){
			$scope.load = true;

			WebService.receberPermissoes(Check.id).success(function(res) {
				$scope.load = false;
				// console.log(res);

				var values = res;

				// console.log(values);

				// console.log('values', values);

				DB.deletePermissao(Check.id).then(function(data){
					// console.log(data);
					for(var i = 0; i < values.length; i++){
						// console.log(values[i].id, values[i].type, values[i].code);
						DB.insertPermissao(Check.id, values[i].id, values[i].type, values[i].code).then(function(data){
							// console.log(data);
							permissao();
						}, function(err){
							console.log(err);
						});
					}

				}, function(err){
					console.log(err);
				});
			})
		}

		$scope.initPresenca = function(){
			$scope.solicitacaoOk = false;
			$scope.imagens = false;
			// console.log('fotos ok:', $scope.imagens);

			$scope.checkOutOk = 2;
			// console.log('checklist ok:', $scope.checkOutOk);
			if(Network.status){
				getPermissoes();
			}else{
				permissao();
			}

			DB.selectMarcacao(Check.id).then(function(data){
				if(data.rows.length){
					var values = data.rows.item(0);
					if(values.status == 1){
						$scope.saida = 1;
						$scope.entrada = 0;
					}else{
						$scope.saida = 0;
						$scope.entrada = 1;
					}
				}else{
					//do nothing
				}
			}, function(err){
				console.log(err);
			});
		}

		var notacoes = [];

		//tela de carregamento desativada
		$scope.send = false;
		$scope.load = false;
		$scope.loadMarcacoes = false;
		$scope.check = Check.verific;
		var funcionario = Check.id;

		$scope.map = false;

		$scope.result = [];

		$scope.conect = Network.status;

		//Verficando localização
		// window.setInterval(Localizacao.posicao(), 5000);

		//Verficando se existem avisos
		// window.setInterval(Message.request(), 5000);

		// $scope.copy = function(){
		// 	var File = new FileSystem.File();
		// 	File.copy();
		// }

		//funçaõ para colorir check list enviado
		$scope.checkList = function(){
			return Check.verificList;
		}

		//função para exibir número de avisos
		$scope.avisos = function(){
			var numAvisos = Message.contAvisos;	
			window.FirebasePlugin.setBadgeNumber(numAvisos);		
			return numAvisos;
		} 

		//função para notificar se os avisos estão disponíveis ou não
		$scope.off = function(){
			if(Network.status == false){
				Popup.marketing();
			}else if(Network.status == true){
				//$window.location.href = '#/marketing';
				$state.go('marketing');
			}else{
				//do nothing
			}
		}

		$scope.image = function(){
			if(Network.status){
				$state.go('images');
			}else{
				Popup.imagesFail();
			}
		}

		//função para notificar se as rotas estão disponíveis ou não
		// $scope.routes = function(){
		// 	if(Network.status == false){
		// 		// Popup.routes();
		// 		$state.go('routes');
		// 	}else if(Network.status == true){
		// 		$state.go('routes');
		// 	}else{
		// 		//do nothing
		// 	}
		// }

		// variáveis para exibição dos dados do Usuário
		$scope.username = Check.nome + ' ' + Check.sobrenome;
		$scope.cargo = Check.cargo;
		$scope.statusFunc = Check.statusFunc;
		$scope.idChecklist = Check.idChecklist;
		//$scope.email = Check.email;
	
		// função  para a chamada do Menu Lateral
		$scope.toggleLeft = function(){
			$ionicSideMenuDelegate.toggleLeft();
		}

		// função para exibir na tela de Controle de Presença o status da conexão com internet
		$scope.mode = function mode(){
			$scope.disp = Network.status ? 'Online' : 'Offline'; //true : false
			$scope.gps = Localizacao.gps;
		}

		function distancia(latInicial, latFinal, lngInicial, lngFinal) {
			// console.log(latInicial, latFinal, lngInicial, lngFinal);
			$scope.metros = null;
			var distancia = 6378140 * Math.acos(Math.cos(Math.PI * (90 - latFinal) / 180) * Math.cos((90 - latInicial) * Math.PI / 180)
					+ Math.sin((90 - latFinal) * Math.PI / 180) * Math.sin((90 - latInicial) * Math.PI / 180) * Math.cos((lngInicial - lngFinal) * Math.PI / 180));
			$scope.metros = distancia;
		}

		//batida somente por localização
		$scope.batidaGPS = function(){
			if(Localizacao.lat != null && Localizacao.longt != null){
				$state.go('marcacoesGPS');
			}else{
				Popup.conexaoGPS();
			}
		}

		function mockInGps(){
			document.addEventListener("deviceready", onDeviceReady, false);

			function onDeviceReady() {
				window.plugins.mockgpschecker.check(successCallback, errorCallback);
			}
			
			function successCallback(result) {
				// console.log(result, 'true - enabled, false - disabled'); // true - enabled, false - disabled
				console.log(result.isMock); // true - enabled, false - disabled
				if (result.isMock == true && Check.bloqueiagpsmock == 1) {
					Popup.bloqueiaGpsMock();
				}else if(result.isMock == true && Check.bloqueiagpsmock == 0){
					$ionicPopup.show({
						'title': Check.nome + ', notamos que você pode estar usando um aplicativo para simular sua localização!',
						'subTitle': 'Desative e desinstale o aplicativo de falsa localização nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									//chamada do PopUp para inserir o código do promotor
									var request = Popup.localGPS();
									//função para verificação do que foi digitado
									request.then(function (res) {
										console.log(res); // Aonde está seu valor
										$scope.local = res;

										if(res){
											notacoes.push("bloqueiaMock");
											checkInGPS();
										}else if(res == undefined){
											//do nothing (Cancelar)
										}else{
											alert("Digite o nome do local! Tente novamente");
										}
									}).catch(function (err) {
										console.log(err);
									})	
								}
							}
						]
					});
				}else if(result.isMock == false){
					//chamada do PopUp para inserir o código do promotor
					var request = Popup.localGPS();
					//função para verificação do que foi digitado
					request.then(function (res) {
						console.log(res); // Aonde está seu valor
						$scope.local = res;

						if(res){
							checkInGPS();
						}else if(res == undefined){
							//do nothing (Cancelar)
						}else{
							alert("Digite o nome do local! Tente novamente");
						}
					}).catch(function (err) {
						console.log(err);
					})	
				}
			}
			
			function errorCallback(error) {
				console.log(error);
			}

		}

		function permissoesHoraInGps(){
			// checando data e hora automáticas (fornecida pela rede)
			window.VerifyAutomaticDateTimeZone.isAutomaticChecked(function(isIt){
				if (isIt[0] == true) {
				// do something
				// console.log(isIt, isIt[0]);
					console.log('true');
					mockInGps();
				} else if(isIt[0] == false && Check.bloqueiahora == 1){
					Popup.bloqueiaHorarioInvalido();
				}else if(isIt[0] == false && Check.bloqueiahora == 0){
					// do something else
					console.log('false');
					$ionicPopup.show({
						'title': Check.nome + ', o horário do seu dispositivo pode não estar sincronizado com a rede!',
						'subTitle': 'Ative Data/ Hora e Fuso Horário automáticos nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaFakeTime");
									mockInGps();
								}
							}
						]
					});
				}
			});	
		}

		//função para efetuar Entrada por GPS (sem QRCode)
		function checkInGPS(){
			//salvando loja escaneada data/hora do Check In
			$scope.lojaIn = "9999 " + $scope.local;
			$scope.currentIn = DateHour.checkIn();

			$scope.notacoes = JSON.stringify(notacoes);
			console.log($scope.notacoes);

			if(Network.status == true){
				//ativando div "enviando"
				$scope.send = true;
				//definindo conexão com internet como Online
				var status = 'Online';
				//salvando variáveis de localização
				$scope.lat = Localizacao.lat;
				$scope.longt = Localizacao.longt;

				//enviando dados para webservice checkInOut
				// WebService.doCheckIn(Check.id, $scope.currentIn, $scope.lojaIn, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).success(function(data) {
				// 	// console.log(data);
				// 	//desativando div "enviando"
				// 	$scope.send = false;
				// 	if(data.result == 'Sucesso'	) {
				// 		//armazena o nome da loja escaneada
				// 		Check.pontocol = $scope.lojaIn;
				// 		Check.currentTimeIn = $scope.currentIn;

				// 		//verifar modo de envio
				// 		Check.statusModeIn = status;

				// 		//limpa loja de Saída (Check Out)
				// 		Check.pontocol2 = 'Não realizado';
				// 		Check.currentTimeOut = '';
				// 		Check.statusModeOut =  '';

				// 		//chamar função para realizar Check List
				// 		// pCheckList();

				// 		// atualiza o ícone para Check In nas telas
				// 		Check.verific = 1;
				// 		$scope.imagens = true;
				// 		Check.checkin();
				// 		notacoes = [];

				// 		$scope.statusMarcacao = 1; //efetuar a Entrada
						
				// 		//salvando no BD SQLite
				// 		DB.selectMarcacao(Check.id).then(function(data){
				// 			if(data.rows.length){
				// 				DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
				// 					// console.log(data);
				// 				}, function(err){
				// 					console.log(err);
				// 				});
				// 			}else{
				// 				DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
				// 					// console.log(data);
				// 				}, function(err){
				// 					console.log(err);
				// 				});
				// 			}
				// 		}, function(err){
				// 			console.log(err);
				// 		});
							
				// 		//$scope.ver = 'Check-In';
				// 		// habilita opção de Check List no menu lateral
				// 		//updateList();

				// 		// atualiza verificação do Check List
				// 		Check.clearCheckList();

				// 		// exibe um alert para informar que o Check In foi realizado com sucesso
				// 		Popup.checkinEfetuado();
						
				// 		$scope.check = Check.verific;
				// 		//enviando email para o funcionário
				// 		var data = {"loja": $scope.lojaIn, "checkin": $scope.currentIn, "funcionario": Check.id};
				// 		WebService.sendEmail(data).success(function(data){
				// 			console.log(data);
				// 			console.log('E-mail de CheckIn enviado!');
				// 		});
						
				// 	}else {
				// 		// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		//alert(data.result);
				// 		Popup.enviarCheckInFail();

				// 		var mensagem = 'Erro ao salvar Check In Online';
				// 		var dataHora = $scope.currentIn;
				// 		var erro = data.result;
				// 		console.log(mensagem, erro, dataHora);

				// 		DB.saveDebug(mensagem, erro, dataHora).then(function(data){
				// 			console.log('Debug Check In Online salvo!');
				// 		}, function(err){
				// 			console.log('Erro ao salvar Debug Check In Online!', err);
				// 		});
				// 	}	            
				// });			
				var currentTimeOut = null;
				//salvando no BD SQLite
				DB.saveCheckIn2(Check.id, $scope.currentIn, currentTimeOut, $scope.lojaIn, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
					//desativando div "enviando"
					$scope.send = false;
					console.log('Check In Salvo');

					//armazena o nome da loja escaneada
					Check.pontocol = $scope.lojaIn;
					Check.currentTimeIn = $scope.currentIn;

					//verifar modo de envio
					Check.statusModeIn = status;

					//limpa loja de Saída (Check Out)
					Check.pontocol2 = 'Não realizado';
					Check.currentTimeOut = '';
					Check.statusModeOut =  '';

					//chamar função para realizar Check List
					// pCheckList();

					// atualiza o ícone para Check In nas telas
					Check.verific = 1;
					$scope.imagens = true;
					Check.checkin();
					notacoes = [];

					$scope.statusMarcacao = 1; //efetuar a Entrada
					$scope.saida = 1;
					$scope.entrada = 0;

					//salvando no BD SQLite
					DB.selectMarcacao(Check.id).then(function(data){
						if(data.rows.length){
							DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}else{
							DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}
					}, function(err){
						console.log(err);
					});
						
					//$scope.ver = 'Check-In';
					// habilita opção de Check List no menu lateral
					//updateList();

					// atualiza verificação do Check List
					Check.clearCheckList();

					// exibe um alert para informar que o Check In foi realizado com sucesso
					Popup.checkinEfetuado();
					
					$scope.check = Check.verific;
				
				}, function(err){
					//desativando div "enviando"
					$scope.send = false;
					console.log(err);
					// exibe que não foi possível enviar os dados para o banco do servidor 
					//alert(data.result);
					Popup.enviarCheckInFail();

					var mensagem = 'Erro ao salvar Check In Online';
					var dataHora = $scope.currentIn;
					var erro = data.result;
					console.log(mensagem, erro, dataHora);

					DB.saveDebug(mensagem, erro, dataHora).then(function(data){
						console.log('Debug Check In Online salvo!');
					}, function(err){
						console.log('Erro ao salvar Debug Check In Online!', err);
					});
				});		
				User.send(); //Envia os dados para o servidor		
			}else if(Network.status == false){
				//ativando div "enviando"
				$scope.send = true;
				//definindo conexão com internet como Offline
				var status = 'Offline';
				$scope.status = status;
				var cartao_pontocol = $scope.lojaIn;
				var currentTimeIn = $scope.currentIn;
				var currentTimeOut = null, latitude = $scope.lat, longitude = $scope.longt;

				console.log(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes);
				//salvando no BD SQLite
				DB.saveCheckIn2(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
					//desativando div "enviando"
					$scope.send = false;
					console.log('Check In Salvo');
					
					//verifar modo de envio
					Check.statusModeIn = $scope.status;

					//armazena nome da Loja escaneada
					Check.pontocol = $scope.lojaIn;
					Check.currentTimeIn = $scope.currentIn;

					//limpa loja de Saída (Check Out)
					Check.pontocol2 = 'Não realizado';
					Check.currentTimeOut = '';
					Check.statusModeOut = '';

					// atualiza verificação do Check List
					// Check.clearCheckList();

					// atualiza o ícone para Check In nas telas
					Check.verific = 1;
					Check.checkin();
					notacoes = [];

					$scope.statusMarcacao = 1; //efetuar a Entrada
					$scope.saida = 1;
					$scope.entrada = 0;

					//salvando no BD SQLite
					DB.selectMarcacao(Check.id).then(function(data){
						if(data.rows.length){
							DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}else{
							DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}
					}, function(err){
						console.log(err);
					});
					
					//$scope.ver = 'Check-In';
					// habilita opção de Check List no menu lateral
					//updateList();
					
					// exibe um alert para informar que o Check In foi realizado com sucesso
					Popup.checkinEfetuado();
					$scope.check = Check.verific;					
				}, function(err){
					//desativando div "enviando"
					$scope.send = false;
					console.log(err);
					Popup.enviarCheckInFail();

					var mensagem = 'Erro ao salvar Check In Offline';
					var dataHora = $scope.currentIn;
					console.log(mensagem, err, dataHora);

					DB.saveDebug(mensagem, err, dataHora).then(function(data){
						console.log('Debug Check In Offline salvo!');
					}, function(err){
						console.log('Erro ao salvar Debug Check In Offline!', err);
					});
				});
			}else{
				// $scope.pontocol = 'Não realizado';
			}	
		}

		function mockOutGps(){
			document.addEventListener("deviceready", onDeviceReady, false);

			function onDeviceReady() {
				window.plugins.mockgpschecker.check(successCallback, errorCallback);
			}
			
			function successCallback(result) {
				// console.log(result, 'true - enabled, false - disabled'); // true - enabled, false - disabled
				console.log(result.isMock); // true - enabled, false - disabled
				if (result.isMock == true && Check.bloqueiagpsmock == 1) {
					Popup.bloqueiaGpsMock();
				}else if(result.isMock == true && Check.bloqueiagpsmock == 0){
					$ionicPopup.show({
						'title': Check.nome + ', notamos que você pode estar usando um aplicativo para simular sua localização!',
						'subTitle': 'Desative e desinstale o aplicativo de falsa localização nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									//chamada do PopUp para inserir o código do promotor
									var request = Popup.localGPS();
									//função para verificação do que foi digitado
									request.then(function (res) {
										console.log(res); // Aonde está seu valor
										$scope.local = res;

										if(res){
											notacoes.push("bloqueiaMock");
											checkOutGPS();
										}else if(res == undefined){
											//do nothing (Cancelar)
										}else{
											alert("Digite o nome do local! Tente novamente");
										}
									}).catch(function (err) {
										console.log(err);
									})
								}
							}
						]
					});
				}else if(result.isMock == false){
					//chamada do PopUp para inserir o código do promotor
					var request = Popup.localGPS();
					//função para verificação do que foi digitado
					request.then(function (res) {
						console.log(res); // Aonde está seu valor
						$scope.local = res;

						if(res){
							checkOutGPS();
						}else if(res == undefined){
							//do nothing (Cancelar)
						}else{
							alert("Digite o nome do local! Tente novamente");
						}
					}).catch(function (err) {
						console.log(err);
					})
				}
			}
			
			function errorCallback(error) {
				console.log(error);
			}

		}

		function permissoesHoraOutGps(){
			// checando data e hora automáticas (fornecida pela rede)
			window.VerifyAutomaticDateTimeZone.isAutomaticChecked(function(isIt){
				if (isIt[0] == true) {
				// do something
				// console.log(isIt, isIt[0]);
					console.log('true');
					mockOutGps();
				} else if(isIt[0] == false && Check.bloqueiahora == 1){
					Popup.bloqueiaHorarioInvalido();
				}else if(isIt[0] == false && Check.bloqueiahora == 0){
					// do something else
					console.log('false');
					$ionicPopup.show({
						'title': Check.nome + ', o horário do seu dispositivo pode não estar sincronizado com a rede!',
						'subTitle': 'Ative Data/ Hora e Fuso Horário automáticos nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaFakeTime");
									mockOutGps();
								}
							}
						]
					});
				}
			});	
		}

		//função para efetuar Saída por GPS (sem QRCode)
		function checkOutGPS(){
			//salvando loja escaneada e data/hora do Check Out
			$scope.lojaOut = "9999 " + $scope.local;
			$scope.currentOut = DateHour.checkOut();
			$scope.notacoes = JSON.stringify(notacoes);
			console.log($scope.notacoes);
			
			if(Network.status == true){
				//ativando div "enviando"
				$scope.send = true;
				//definindo conexão com internet como Online	
				var status = 'Online';
				//salvando variáveis de localização
				$scope.lat2 = Localizacao.lat;
				$scope.longt2 = Localizacao.longt;
				
				// $scope.notacoes = JSON.stringify(notacoes);
				// console.log($scope.notacoes);

				//enviando dados para webservice checkInOut
				// WebService.doCheckOut(Check.id, $scope.currentOut, $scope.lojaOut, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).success(function(data) {
				// 	// console.log(data);
				// 	//desativando div "enviando"
				// 	$scope.send = false;
				// 	if(data.result == 'Sucesso') {									   									                
				// 		//armazena nome da Loja escaneada
				// 		Check.pontocol2 = $scope.lojaOut;
				// 		Check.currentTimeOut = $scope.currentOut;

				// 		//verifar modo de envio
				// 		Check.statusModeOut = status;

				// 		// atualiza o ícone para Check Out nas telas
				// 		Check.verific = 2;
				// 		Check.checkout();
				// 		$scope.imagens = false;
				// 		$scope.statusMarcacao = 2; //efetuar a Saída

				// 		notacoes = [];
						
				// 		//salvando no BD SQLite
				// 		DB.selectMarcacao(Check.id).then(function(data){
				// 			if(data.rows.length){
				// 				DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
				// 					// console.log(data);
				// 				}, function(err){
				// 					console.log(err);
				// 				});
				// 			}else{
				// 				DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
				// 					// console.log(data);
				// 				}, function(err){
				// 					console.log(err);
				// 				});
				// 			}
				// 		}, function(err){
				// 			console.log(err);
				// 		});
								
				// 		// dasabilita opção de Check List no menu lateral
				// 		//$scope.ver = 'Check-Out';
				// 		//updateList();

				// 		// atualiza verificação do Check List
				// 		//Check.clearCheckList();
								
				// 		//chamar função para realizar Check List
				// 		// pCheckList();
								
				// 		// exibe um alert para informar que o Check Out foi realizado com sucesso
				// 		Popup.checkoutEfetuado();
				// 		$scope.check = Check.verific;
				// 		//enviando email para o funcionário
				// 		var data = {"loja": $scope.lojaOut, "checkout": $scope.currentOut, "funcionario": Check.id};
				// 		WebService.sendEmail(data).success(function(data){
				// 			console.log(data);
				// 			console.log('E-mail de CheckOut enviado!');
				// 		});
				// 	}else {
				// 		// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		// console.log(data.result);
				// 		Popup.enviarCheckOutFail();

				// 		var mensagem = 'Erro ao salvar Check Out Online';
				// 		var dataHora = $scope.currentOut;
				// 		var erro = data.result;
				// 		console.log(mensagem, erro, dataHora);

				// 		DB.saveDebug(mensagem, erro, dataHora).then(function(data){
				// 			console.log('Debug Check Out Online salvo!');
				// 		}, function(err){
				// 			console.log('Erro ao salvar Debug Check Out Online!', err);
				// 		});
				// 	}	            
				// });
				var currentTimeIn = null;
				//salvando no BD SQLite
				DB.saveCheckOut2(Check.id, currentTimeIn, $scope.currentOut, $scope.lojaOut, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
					//desativando div "enviando"
					$scope.send = false;
					console.log('Check Out Salvo');

					//armazena nome da Loja escaneada
					Check.pontocol2 = $scope.lojaOut;
					Check.currentTimeOut = $scope.currentOut;

					//verifar modo de envio
					Check.statusModeOut = status;

					// atualiza o ícone para Check Out nas telas
					Check.verific = 2;
					Check.checkout();
					$scope.imagens = false;
					$scope.statusMarcacao = 2; //efetuar a Saída
					$scope.saida = 0;
					$scope.entrada = 1;

					notacoes = [];
					
					//salvando no BD SQLite
					DB.selectMarcacao(Check.id).then(function(data){
						if(data.rows.length){
							DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}else{
							DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}
					}, function(err){
						console.log(err);
					});
							
					// dasabilita opção de Check List no menu lateral
					//$scope.ver = 'Check-Out';
					//updateList();

					// atualiza verificação do Check List
					//Check.clearCheckList();
							
					//chamar função para realizar Check List
					// pCheckList();
							
					// exibe um alert para informar que o Check Out foi realizado com sucesso
					Popup.checkoutEfetuado();
					$scope.check = Check.verific;

				}, function(err){
					//desativando div "enviando"
					$scope.send = false;
					console.log(err);

					// exibe que não foi possível enviar os dados para o banco do servidor 
					// console.log(data.result);
					Popup.enviarCheckOutFail();

					var mensagem = 'Erro ao salvar Check Out Online';
					var dataHora = $scope.currentOut;
					var erro = data.result;
					console.log(mensagem, erro, dataHora);

					DB.saveDebug(mensagem, erro, dataHora).then(function(data){
						console.log('Debug Check Out Online salvo!');
					}, function(err){
						console.log('Erro ao salvar Debug Check Out Online!', err);
					});

				});
				User.send(); //Envia os dados para o servidor
			}else if(Network.status == false){
				//ativando div "enviando"
				$scope.send = true;
				//definindo conexão com internet como Offline
				var status = 'Offline';
				$scope.status = status;
				var cartao_pontocol = $scope.lojaOut;
				var currentTimeOut = $scope.currentOut;
				var currentTimeIn = null, latitude = $scope.lat2, longitude = $scope.longt2;

				console.log(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes);
				//salvando no BD SQLite
				DB.saveCheckOut2(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
					//desativando div "enviando"
					$scope.send = false;
					console.log('Check Out Salvo');
					
					//verifar modo de envio
					Check.statusModeOut = $scope.status;

					//armazena o nome da loja escaneada
					Check.pontocol2 = $scope.lojaOut;
					Check.currentTimeOut = $scope.currentOut;
						
					// atualiza o ícone para Check Out nas telas
					Check.verific = 2;
					Check.checkout();
					notacoes = [];

					$scope.statusMarcacao = 2; //efetuar a Saída
					$scope.saida = 0;
					$scope.entrada = 1;

					//salvando no BD SQLite
					DB.selectMarcacao(Check.id).then(function(data){
						if(data.rows.length){
							DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}else{
							DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}
					}, function(err){
						console.log(err);
					});
						
					//chamar função para realizar Check List
					// pCheckList();
						
					// dasabilita opção de Check List no menu lateral
					//$scope.ver = 'Check-Out';
					//updateList();

					// atualiza verificação do Check List
					//Check.clearCheckList();

					//exibe um alert para informar que o Check Out foi realizado com sucesso
					Popup.checkoutEfetuado();
					$scope.check = Check.verific;
				}, function(err){
					//desativando div "enviando"
					$scope.send = false;
					console.log(err);
					Popup.enviarCheckOutFail();

					var mensagem = 'Erro ao salvar Check Out Offline';
					var dataHora = $scope.currentOut;
					console.log(mensagem, err, dataHora);

					DB.saveDebug(mensagem, err, dataHora).then(function(data){
						console.log('Debug Check Out Offline salvo!');
					}, function(err){
						console.log('Erro ao salvar Debug Check Out Offline!', err);
					});
				});
			}else{
				//do nothing
			}
		}

		$scope.marcacaoGPS = function(batida){	
			notacoes = [];
			if(batida == 0){
				DB.selectMarcacao(Check.id).then(function(data){
					if(data.rows.length){
						var values = data.rows.item(0);
						if(values.status == 1){
							$scope.saida = 1;
							$scope.entrada = 0;

							$ionicPopup.alert({
								'title': 'Confirmar nova Entrada?',
								'subTitle': 'Uma batida de Entrada já foi realizada.',
								'template': '',
								'buttons': [
									{
										'text': 'Cancelar',
										'type': 'button-assertive',
										'onTap': function(e){
											//Do nothing
										}
									},
									{
										'text': 'Confirmar',
										'type': 'button-balanced',
										'onTap': function(e){
											// //chamada do PopUp para inserir o código do promotor
											// var request = Popup.localGPS();
											// //função para verificação do que foi digitado
											// request.then(function (res) {
											// 	console.log(res); // Aonde está seu valor
											// 	$scope.local = res;

											// 	if(res){
													permissoesHoraInGps();
											// 	}else if(res == undefined){
											// 		//do nothing (Cancelar)
											// 	}else{
											// 		alert("Digite o nome do local! Tente novamente");
											// 	}
											// }).catch(function (err) {
											// 	console.log(err);
											// })	
										}
									}
								]
							});
						}else{
							//chamada do PopUp para inserir o código do promotor
							// var request = Popup.localGPS();
							// //função para verificação do que foi digitado
							// request.then(function (res) {
							// 	console.log(res); // Aonde está seu valor
							// 	$scope.local = res;

							// 	if(res){
									permissoesHoraInGps();
							// 	}else if(res == undefined){
							// 		//do nothing (Cancelar)
							// 	}else{
							// 		alert("Digite o nome do local! Tente novamente");
							// 	}
							// }).catch(function (err) {
							// 	console.log(err);
							// })	
						}
					}else{
						// //chamada do PopUp para inserir o código do promotor
						// var request = Popup.localGPS();
						// //função para verificação do que foi digitado
						// request.then(function (res) {
						// 	console.log(res); // Aonde está seu valor
						// 	$scope.local = res;

						// 	if(res){
								permissoesHoraInGps();
						// 	}else if(res == undefined){
						// 		//do nothing (Cancelar)
						// 	}else{
						// 		alert("Digite o nome do local! Tente novamente");
						// 	}
						// }).catch(function (err) {
						// 	console.log(err);
						// })	
					}
				}, function(err){
					console.log(err);
				});
			}else{
				DB.selectMarcacao(Check.id).then(function(data){
					if(data.rows.length){
						var values = data.rows.item(0);
						if(values.status == 2){
							$scope.saida = 0;
							$scope.entrada = 1;
							
							$ionicPopup.alert({
								'title': 'Confirmar nova Saída?',
								'subTitle': 'Uma batida de Saída já foi realizada.',
								'template': '',
								'buttons': [
									{
										'text': 'Cancelar',
										'type': 'button-assertive',
										'onTap': function(e){
											//Do nothing
										}
									},
									{
										'text': 'Confirmar',
										'type': 'button-balanced',
										'onTap': function(e){
											// //chamada do PopUp para inserir o código do promotor
											// var request = Popup.localGPS();
											// //função para verificação do que foi digitado
											// request.then(function (res) {
											// 	console.log(res); // Aonde está seu valor
											// 	$scope.local = res;

											// 	if(res){
													permissoesHoraOutGps();
											// 	}else if(res == undefined){
											// 		//do nothing (Cancelar)
											// 	}else{
											// 		alert("Digite o nome do local! Tente novamente");
											// 	}
											// }).catch(function (err) {
											// 	console.log(err);
											// })
										}
									}
								]
							});
						}else{
							// //chamada do PopUp para inserir o código do promotor
							// var request = Popup.localGPS();
							// //função para verificação do que foi digitado
							// request.then(function (res) {
							// 	console.log(res); // Aonde está seu valor
							// 	$scope.local = res;

							// 	if(res){
									permissoesHoraOutGps();
							// 	}else if(res == undefined){
							// 		//do nothing (Cancelar)
							// 	}else{
							// 		alert("Digite o nome do local! Tente novamente");
							// 	}
							// }).catch(function (err) {
							// 	console.log(err);
							// })
						}
					}else{
						// //chamada do PopUp para inserir o código do promotor
						// var request = Popup.localGPS();
						// //função para verificação do que foi digitado
						// request.then(function (res) {
						// 	console.log(res); // Aonde está seu valor
						// 	$scope.local = res;

						// 	if(res){
								permissoesHoraOutGps();
						// 	}else if(res == undefined){
						// 		//do nothing (Cancelar)
						// 	}else{
						// 		alert("Digite o nome do local! Tente novamente");
						// 	}
						// }).catch(function (err) {
						// 	console.log(err);
						// })
					}
				}, function(err){
					console.log(err);
				});		
			}
		}

		function mockIn(){
			document.addEventListener("deviceready", onDeviceReady, false);

			function onDeviceReady() {
				window.plugins.mockgpschecker.check(successCallback, errorCallback);
			}
			
			function successCallback(result) {
				// console.log(result, 'true - enabled, false - disabled'); // true - enabled, false - disabled
				console.log(result.isMock); // true - enabled, false - disabled
				if (result.isMock == true && Check.bloqueiagpsmock == 1) {
					Popup.bloqueiaGpsMock();
				}else if(result.isMock == true && Check.bloqueiagpsmock == 0){
					$ionicPopup.show({
						'title': Check.nome + ', notamos que você pode estar usando um aplicativo para simular sua localização!',
						'subTitle': 'Desative e desinstale o aplicativo de falsa localização nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaMock");
									gpsCheckIn();
								}
							}
						]
					});
				}else if(result.isMock == false){
					gpsCheckIn();
				}
			}
			
			function errorCallback(error) {
				console.log(error);
			}

		}

		function permissoesHoraIn(){
			// checando data e hora automáticas (fornecida pela rede)
			window.VerifyAutomaticDateTimeZone.isAutomaticChecked(function(isIt){
				if (isIt[0] == true) {
				// do something
				// console.log(isIt, isIt[0]);
					console.log('true');
					mockIn();
				} else if(isIt[0] == false && Check.bloqueiahora == 1){
					Popup.bloqueiaHorarioInvalido();
				}else if(isIt[0] == false && Check.bloqueiahora == 0){
					// do something else
					console.log('false');
					$ionicPopup.show({
						'title': Check.nome + ', o horário do seu dispositivo pode não estar sincronizado com a rede!',
						'subTitle': 'Ative Data/ Hora e Fuso Horário automáticos nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaFakeTime");
									mockIn();
								}
							}
						]
					});
				}
			});	
		}

		function mockOut(){
			document.addEventListener("deviceready", onDeviceReady, false);

			function onDeviceReady() {
				window.plugins.mockgpschecker.check(successCallback, errorCallback);
			}
			
			function successCallback(result) {
				// console.log(result, 'true - enabled, false - disabled'); // true - enabled, false - disabled
				console.log(result.isMock); // true - enabled, false - disabled
				if (result.isMock == true && Check.bloqueiagpsmock == 1) {
					Popup.bloqueiaGpsMock();
				}else if(result.isMock == true && Check.bloqueiagpsmock == 0){
					$ionicPopup.show({
						'title': Check.nome + ', notamos que você pode estar usando um aplicativo para simular sua localização!',
						'subTitle': 'Desative e desinstale o aplicativo de falsa localização nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaMock");
									gpsCheckOut();
								}
							}
						]
					});
				}else if(result.isMock == false){
					gpsCheckOut();
				}
			}
			
			function errorCallback(error) {
				console.log(error);
			}

		}

		function permissoesHoraOut(){
			// checando data e hora automáticas (fornecida pela rede)
			window.VerifyAutomaticDateTimeZone.isAutomaticChecked(function(isIt){
				if (isIt[0] == true) {
				// do something
				// console.log(isIt, isIt[0]);
					console.log('true');
					mockOut();
				} else if(isIt[0] == false && Check.bloqueiahora == 1){
					Popup.bloqueiaHorarioInvalido();
				}else if(isIt[0] == false && Check.bloqueiahora == 0){
					// do something else
					console.log('false');
					$ionicPopup.show({
						'title': Check.nome + ', o horário do seu dispositivo pode não estar sincronizado com a rede!',
						'subTitle': 'Ative Data/ Hora e Fuso Horário automáticos nas configurações de seu dispositivo.',
						'buttons': [
							{
								'text': 'OK',
								'type': 'button-balanced',
								'onTap': function(e){
									notacoes.push("bloqueiaFakeTime");
									mockOut();
								}
							}
						]
					});
				}
			});	
		}

		$scope.autentic = 3;
		// função para realizar Check In
		$scope.checkin = function(){
			notacoes = [];
			// Localizacao.posicao();
			DB.selectMarcacao(Check.id).then(function(data){
				if(data.rows.length){
					var values = data.rows.item(0);
					if(values.status == 1){
						$scope.saida = 1;
						$scope.entrada = 0;
						
						$ionicPopup.alert({
							'title': 'Confirmar nova Entrada?',
							'subTitle': 'Uma batida de Entrada já foi realizada.',
							'template': '',
							'buttons': [
								{
									'text': 'Cancelar',
									'type': 'button-assertive',
									'onTap': function(e){
										//Do nothing
									}
								},
								{
									'text': 'Confirmar',
									'type': 'button-balanced',
									'onTap': function(e){
										permissoesHoraIn();
									}
								}
							]
						});
					}else{
						permissoesHoraIn();
					}
				}else{
					permissoesHoraIn();
				}
			}, function(err){
				console.log(err);
			});
		};

		function gpsCheckIn(){
			if(Check.gpsAtivo == 1){
				if(Localizacao.lat == null && Localizacao.longt == null){
					// notifica que não foi possível encontrar a localização do dispositivo
					Popup.conexaoGPS();
				}else{
					if(Check.cargoQuinhentos == 1 && Check.metrosAtivo == 1){
						distancia(Localizacao.lat, Check.lojaLat, Localizacao.longt, Check.lojaLong);
						if($scope.metros <= 500 || (Localizacao.lat == null && Localizacao.longt == null)){
							qrCheckIn();
						}else{
							Popup.metrosInvalido();
						}
					}else{
						qrCheckIn();
					}
				}
			}else{
				qrCheckIn();
			}
		}

		function qrCheckIn(){
			$scope.autentic = 1;
			QRcode();
		}

		// função para realizar Check Out
		$scope.checkout = function(){
			notacoes = [];
			// Localizacao.posicao();
			DB.selectMarcacao(Check.id).then(function(data){
				if(data.rows.length){
					var values = data.rows.item(0);
					if(values.status == 2){
						$scope.saida = 0;
						$scope.entrada = 1;
						
						$ionicPopup.alert({
							'title': 'Confirmar nova Saída?',
							'subTitle': 'Uma batida de Saída já foi realizada.',
							'template': '',
							'buttons': [
								{
									'text': 'Cancelar',
									'type': 'button-assertive',
									'onTap': function(e){
										//Do nothing
									}
								},
								{
									'text': 'Confirmar',
									'type': 'button-balanced',
									'onTap': function(e){
										permissoesHoraOut();
									}
								}
							]
						});
					}else{
						permissoesHoraOut();
					}
				}else{
					permissoesHoraOut();
				}
			}, function(err){
				console.log(err);
			});		
		}
		
		function gpsCheckOut(){
			if(Check.gpsAtivo == 1){
				if(Localizacao.lat == null && Localizacao.longt == null){
					// notifica que não foi possível encontrar a localização do dispositivo
					Popup.conexaoGPS();
				}else{
					if(Check.cargoQuinhentos == 1 && Check.metrosAtivo == 1){
						distancia(Localizacao.lat, Check.lojaLat, Localizacao.longt, Check.lojaLong);
						if($scope.metros <= 500 || (Localizacao.lat == null && Localizacao.longt == null)){
							qrCheckOut();
						}else{
							Popup.metrosInvalido();
						}
					}else{
						qrCheckOut();
					}
				}
			}else{
				qrCheckOut();
			}
		}

		function qrCheckOut(){
			$scope.autentic = 2;
			QRcode();
		}

		// função para receber as entradas e saídas diárias do funcionário
		$scope.report = function(){
			$scope.fail = false;
			if(Network.status){
				//ativando div "carregando"
				$scope.load = true;
				var begin, end, today = new Date(), day = today.getDate(), month = (today.getMonth() + 1);

				begin = end = today.getFullYear() +"-"+ (month < 10 ? '0' + month:month) +"-"+ (day < 10 ? '0' + day:day);
				
				$scope.result = [];
				WebService.report(Check.id, begin, end, true).success(function(data) {
					// console.log(data);
					$scope.result = Order.format(data);

					//desativando div "carregando"
					$scope.load = false;
					// fazer um FOR 
					// console.log(result[i].loja, result[i].checkin, result[i].checkout);
					if($scope.result.length == 0){
						$scope.fail = true;
						$scope.noReports = 'Nenhum registro do dia atual na nuvem do LEDi Promoter!'
					}
						            
			    });
			}else{
				Popup.reportFail();
			}	
		}
				
		function permissao(){
			//modulos
			//1 - fotos
			//2 - checklist

			//salvando no BD SQLite
			// DB.insertPermissao('1, 2, 3', 'cargo', 'fotos').then(function(data){
			// 	console.log(data);
			// }, function(err){
			// 	console.log(err);
			// });

			// DB.insertPermissao('4, 5', 'loja', 'fotos').then(function(data){
			// 	console.log(data);
			// }, function(err){
			// 	console.log(err);
			// });

			// var arrayPermissoes = ['fotos', 'checklist'];

			// for(var i = 0; i < arrayPermissoes.length; i++){
				//seleconando do BD SQLite permissões
				DB.selectPermissao(Check.id).then(function(data){
					if(data.rows.length){
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
												
							var permissoes = values.id;
							var res = permissoes.split(",");

							// console.log(values);
							// console.log('id: ', values.id, '. tipo: ', values.tipo);

							if(values.tipo == 'cargo'){		
								// console.log(values);		
								// console.log('tamanho: ', res.length, res[0], res[1]);
								// console.log('values: ', values.id, res);

								if(res.length > 1){
									for(var j = 0; j < res.length; j++){
										// console.log(res[i], Check.idCargo);
										if(res[j] == Check.idCargo){
											if(values.modulo == 'fotos'){
												$scope.imagens = true;
												// console.log('fotos ok: ', $scope.imagens);
											}else if(values.modulo == 'checklist'){
												$scope.checkOutOk = 1;
												// console.log('checklist ok: ', $scope.checkOutOk);
											}else{
												//do nothing
											}
										}
									}
								}else{
									// console.log(res[0]);
									if(res[0] == Check.idCargo){
										if(values.modulo == 'fotos'){
											$scope.imagens = true;
											// console.log('fotos ok: ', $scope.imagens);
										}else if(values.modulo == 'checklist'){
											$scope.checkOutOk = 1;
											// console.log('checklist ok: ', $scope.checkOutOk);
										}else{
											//do nothing
										}
									}
								}
							// }else if(values.tipo == 'loja'){
							// 	var permissoes = values.id;
							// 	var res = permissoes.split(",");
							// 	console.log(res);

							// 	for(var i = 0; i < res.length; i++){
							// 		console.log(res[i]);
							// 	}
							}else{
								//do nothing
							}
						}
					}else{
						//nothing
					}
				}, function(err){
					console.log(err);
				});
			// }
		}

		//Solicitação
		// if(Check.cargo == "Supervisor Regional" || Check.cargo == "Supervisor Geral" || Check.cargo == "SUPERVISOR REGIONAL" || Check.cargo == "SUPERVISOR GERAL"){
		// 	$scope.solicitacaoOk = true;
		// 	$scope.imagens = true;
		// // }else{
		// 	$scope.solicitacaoOk = false;
		// 	$scope.imagens = false;
		// 	console.log('fotos ok:', $scope.imagens);
		// }

		// Check List sempre disponível
		// não disponível para Promotores
		// // if(Check.cargo == "Promotor"){
		// 	$scope.checkOutOk = 2;
		// 	console.log('checklist ok:', $scope.checkOutOk);
		// }else{
		// 	$scope.checkOutOk = 1;
		// }

		$scope.avisoList = function(){
			if($scope.checkOutOk == 1 && Check.verificList == 1 && Check.verific == 2){
				Popup.checkListRespondido();
			}else if($scope.checkOutOk == 1){
				Popup.realizarCheckList();
			}else{
				//do nothing
			}
		}

		// função para procedimento de Check List
		function pCheckList(){
			if(Check.cargo == "Promotor"){
				$scope.checkOutOk = 2;
			}else{
				Popup.pCheckList();
			}
		}
		
		//Variáveis para exibição da loja e data/ horário de CheckIn e Check Out
		$scope.pontocol = Check.pontocol || 'Não realizado';
		$scope.pontocol2 = Check.pontocol2 || 'Não realizado';

		$scope.currentTimeIn = Check.currentTimeIn || '';
		$scope.currentTimeOut = Check.currentTimeOut || '';

		$scope.statusModeIn = Check.statusModeIn || '';
		$scope.statusModeOut = Check.statusModeOut || '';

		/* função para a chamada do QRCode scanner e a validação de Check In, Check Out e Check List, 
		   em modo Online (com conexão com Internet) e em modo Offline (sem conexão com internet) */
		function QRcode(){
			// seleciona arquivo
			User.getInfo();
			document.addEventListener('deviceready', function () {
				cordova.plugins.barcodeScanner.scan(function (result) {
					var pattern = new RegExp(/[0-9]{1,10} \w+/g);
					var qrtexto = result.text;
					console.log(qrtexto);

					var resultado = qrtexto.split(" ");
					console.log(resultado, resultado[0], resultado[1]);
					// validando loja
					DB.selectLojas().then(function(data){
						if(data.rows.length){
							var invalido = 0;
							//console.log(data.rows);
							for(var i = 0; i < data.rows.length; i++){
								var values = data.rows.item(i);

								if(values.codigo == resultado[0]){
									invalido = 0;
									i = data.rows.length;
									
									console.log('Loja encontrada', values.codigo);
									if($scope.autentic == 1){
										if(pattern.test(qrtexto) == true){
											//salvando loja escaneada data/hora do Check In
											$scope.lojaIn = result.text;
											$scope.currentIn = DateHour.checkIn();
				
											//salvando variáveis de localização
											$scope.lat = Localizacao.lat;
											$scope.longt = Localizacao.longt;
				
											$scope.notacoes = JSON.stringify(notacoes);
											console.log($scope.notacoes);
				
											if(Network.status == true){
												//ativando div "enviando"
												$scope.send = true;
												//definindo conexão com internet como Online
												var status = 'Online';
				
												//enviando dados para webservice checkInOut
												// WebService.doCheckIn(Check.id, $scope.currentIn, $scope.lojaIn, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).success(function(data) {
												// 	// console.log(data);
												// 	//desativando div "enviando"
												// 	$scope.send = false;
												// 	if(data.result == 'Sucesso'	) {
												// 		//armazena o nome da loja escaneada
												//         Check.pontocol = $scope.lojaIn;
												//         Check.currentTimeIn = $scope.currentIn;
				
												// 		//verifar modo de envio
												// 		Check.statusModeIn = status;
				
												// 		//limpa loja de Saída (Check Out)
												// 		Check.pontocol2 = 'Não realizado';
												// 		Check.currentTimeOut = '';
												// 		Check.statusModeOut =  '';
				
												// 	    // atualiza o ícone para Check In nas telas
												// 		Check.verific = 1;
												// 		$scope.check = Check.verific;
												// 		notacoes = [];
												// 		Check.checkin();
												// 		permissao();
																									
												// 	    //$scope.ver = 'Check-In';
												// 		// habilita opção de Check List no menu lateral
												// 		//updateList();
				
												// 		// atualiza verificação do Check List
												// 		Check.clearCheckList();
				
												// 		$scope.statusMarcacao = 1; //efetuar a Entrada
														
												// 		//salvando no BD SQLite
												// 		DB.selectMarcacao(Check.id).then(function(data){
												// 			if(data.rows.length){
												// 				DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
												// 					// console.log(data);
												// 				}, function(err){
												// 					console.log(err);
												// 				});
												// 			}else{
												// 				DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
												// 					// console.log(data);
												// 				}, function(err){
												// 					console.log(err);
												// 				});
												// 			}
												// 		}, function(err){
												// 			console.log(err);
												// 		});
				
												//         // exibe um alert para informar que o Check In foi realizado com sucesso
												// 		Popup.checkinEfetuado();
														
												// 		// if($scope.lat == null && $scope.longt == null){
												// 		// 	// notifica que não foi possível encontrar a localização do dispositivo
												// 		// 	Popup.conexaoGPS();
												// 		// }
				
												// 		//enviando email para o funcionário
												//         var data = {"loja": $scope.lojaIn, "checkin": $scope.currentIn, "funcionario": Check.id};
												//         WebService.sendEmail(data).success(function(data){
												//         	//alert(data);
												// 			console.log('E-mail de CheckIn enviado!');
												//         });
												//     }else {
												//     	// exibe que não foi possível enviar os dados para o banco do servidor 
												//         //alert(data.result);
												// 		Popup.enviarCheckInFail();
				
												// 		var mensagem = 'Erro ao salvar Check In Online';
												// 		var dataHora = $scope.currentIn;
												// 		var erro = data.result;
												// 		console.log(mensagem, erro, dataHora);
				
												// 		DB.saveDebug(mensagem, erro, dataHora).then(function(data){
												// 			console.log('Debug Check In Online salvo!');
												// 		}, function(err){
												// 			console.log('Erro ao salvar Debug Check In Online!', err);
												// 		});
												//     }	            
												// });							
												var currentTimeOut = null;
												//salvando no BD SQLite
												DB.saveCheckIn2(Check.id, $scope.currentIn, currentTimeOut, $scope.lojaIn, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
													//desativando div "enviando"
													$scope.send = false;
													console.log('Check In Salvo');
				
													//armazena o nome da loja escaneada
													Check.pontocol = $scope.lojaIn;
													Check.currentTimeIn = $scope.currentIn;
				
													//verifar modo de envio
													Check.statusModeIn = status;
				
													//limpa loja de Saída (Check Out)
													Check.pontocol2 = 'Não realizado';
													Check.currentTimeOut = '';
													Check.statusModeOut =  '';
				
													// atualiza o ícone para Check In nas telas
													Check.verific = 1;
													$scope.check = Check.verific;
													notacoes = [];
													Check.checkin();
													permissao();
																								
													//$scope.ver = 'Check-In';
													// habilita opção de Check List no menu lateral
													//updateList();
				
													// atualiza verificação do Check List
													Check.clearCheckList();
				
													$scope.statusMarcacao = 1; //efetuar a Entrada
													$scope.saida = 1;
													$scope.entrada = 0;
													
													//salvando no BD SQLite
													DB.selectMarcacao(Check.id).then(function(data){
														if(data.rows.length){
															DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}else{
															DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}
													}, function(err){
														console.log(err);
													});
				
													// exibe um alert para informar que o Check In foi realizado com sucesso
													Popup.checkinEfetuado();
													
													// if($scope.lat == null && $scope.longt == null){
													// 	// notifica que não foi possível encontrar a localização do dispositivo
													// 	Popup.conexaoGPS();
													// }
				
												}, function(err){
													//desativando div "enviando"
													$scope.send = false;
													// exibe que não foi possível enviar os dados para o banco do servidor 
													//alert(data.result);
													Popup.enviarCheckInFail();
				
													var mensagem = 'Erro ao salvar Check In Online';
													var dataHora = $scope.currentIn;
													var erro = data.result;
													console.log(mensagem, erro, dataHora);
				
													DB.saveDebug(mensagem, erro, dataHora).then(function(data){
														console.log('Debug Check In Online salvo!');
													}, function(err){
														console.log('Erro ao salvar Debug Check In Online!', err);
													});
												});
												User.send(); //Envia os dados para o servidor
											}else if(Network.status == false){
												//ativando div "enviando"
												$scope.send = true;
												//definindo conexão com internet como Offline
												var status = 'Offline';
												$scope.status = status;
												var cartao_pontocol = $scope.lojaIn;
												var currentTimeIn = $scope.currentIn;
												var currentTimeOut = null, latitude = $scope.lat, longitude = $scope.longt;
				
												console.log(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, latitude, longitude, status, $scope.notacoes);
												//salvando no BD SQLite
												DB.saveCheckIn2(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
													//desativando div "enviando"
													$scope.send = false;
													console.log('Check In Salvo');
													
													//verifar modo de envio
													Check.statusModeIn = $scope.status;
				
													//armazena nome da Loja escaneada
													Check.pontocol = $scope.lojaIn;
													Check.currentTimeIn = $scope.currentIn;
				
													//limpa loja de Saída (Check Out)
													Check.pontocol2 = 'Não realizado';
													Check.currentTimeOut = '';
													Check.statusModeOut = '';
				
													// atualiza verificação do Check List
													Check.clearCheckList();
				
													// atualiza o ícone para Check In nas telas
													Check.verific = 1;
													Check.checkin();
													permissao();
													notacoes = [];
													
													// if($scope.lat == null && $scope.longt == null){
													// 	// notifica que não foi possível encontrar a localização do dispositivo
													// 	Popup.conexaoGPS();
													// }	
				
													$scope.statusMarcacao = 1; //efetuar a Entrada
													$scope.saida = 1;
													$scope.entrada = 0;
													
													//salvando no BD SQLite
													DB.selectMarcacao(Check.id).then(function(data){
														if(data.rows.length){
															DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}else{
															DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}
													}, function(err){
														console.log(err);
													});
				
													//$scope.ver = 'Check-In';
													// habilita opção de Check List no menu lateral
													//updateList();
													
													// exibe um alert para informar que o Check In foi realizado com sucesso
													Popup.checkinEfetuado();
													$scope.check = Check.verific;
												}, function(err){
													//desativando div "enviando"
													$scope.send = false;
													console.log(err);
													Popup.enviarCheckInFail();
				
													var mensagem = 'Erro ao salvar Check In Offline';
													var dataHora = $scope.currentIn;
													console.log(mensagem, err, dataHora);
				
													DB.saveDebug(mensagem, err, dataHora).then(function(data){
														console.log('Debug Check In Offline salvo!');
													}, function(err){
														console.log('Erro ao salvar Debug Check In Offline!', err);
													});
												});
				
												// //armazenando informações para serem salvas no arquivo.json
												// var info = {
												// 	"latitude": null,
												// 	"longitude": null,
												// 	"cartao_pontocol": $scope.lojaIn,
												// 	"funcionario": Check.id,
												// 	"checkin": currentTimeIn,
												// 	"checkout": "",
												// 	"status" : status
												// }
				
												//salva os dados no arquivo.json
												// User.save(info, function (data, err) {
												// 	//desativando div "enviando"
												// 	$scope.send = false;
												// 	// console.log(data, err);
												// 	if(err){
												// 		console.log(err);
												// 		Popup.enviarCheckInFail();
												// 	}else{
												// 		//verifar modo de envio
												// 		Check.statusModeIn = status;
				
												// 		//armazena nome da Loja escaneada
												// 		Check.pontocol = $scope.lojaIn;
												// 		Check.currentTimeIn = currentTimeIn;
				
												// 		//limpa loja de Saída (Check Out)
												// 		Check.pontocol2 = 'Não realizado';
												// 		Check.currentTimeOut = '';
												// 		Check.statusModeOut = '';
				
												// 		// atualiza verificação do Check List
												// 		Check.clearCheckList();
				
												// 		// atualiza o ícone para Check In nas telas
												// 		Check.verific = 1;
												// 		Check.checkin();
														
												// 		//$scope.ver = 'Check-In';
												// 		// habilita opção de Check List no menu lateral
												// 		//updateList();
														
												// 		// exibe um alert para informar que o Check In foi realizado com sucesso
												// 		Popup.checkinEfetuado();
												// 		$scope.check = Check.verific;
												// 	}
												// })
											}else{
												// $scope.pontocol = 'Não realizado';
											}	
										}else if(!result.text){
											// alert("Não foi possível escanear, tente novamente!");
										}else{
											Popup.qrcodeInvalido();
										}
									} else if ($scope.autentic == 2){
										if(pattern.test(qrtexto) == true){
											//salvando loja escaneada e data/hora do Check Out
											$scope.lojaOut = result.text;
											$scope.currentOut = DateHour.checkOut();
				
											//salvando variáveis de localização
											$scope.lat2 = Localizacao.lat2;
											$scope.longt2 = Localizacao.longt2;
				
											$scope.notacoes = JSON.stringify(notacoes);
											console.log($scope.notacoes);
											
											if(Network.status == true){
												//ativando div "enviando"
												$scope.send = true;
												//definindo conexão com internet como Online	
												var status = 'Online';
												//salvando variáveis de localização
												$scope.lat2 = Localizacao.lat;
												$scope.longt2 = Localizacao.longt;
														
												//enviando dados para webservice checkInOut
												// WebService.doCheckOut(Check.id, $scope.currentOut, $scope.lojaOut, Localizacao.lat, Localizacao.longt, status,$scope.notacoes).success(function(data) {
												// 	// console.log(data);
												// 	//desativando div "enviando"
												// 	$scope.send = false;
												//     if(data.result == 'Sucesso') {									   									                
												// 		//armazena nome da Loja escaneada
												//         Check.pontocol2 = $scope.lojaOut;
												// 		Check.currentTimeOut = $scope.currentOut;
				
												// 		//verifar modo de envio
												// 		Check.statusModeOut = status;
				
												//         // atualiza o ícone para Check Out nas telas
												// 		Check.verific = 2;
												// 		$scope.check = Check.verific;
												// 		Check.checkout();
												// 		notacoes = [];
												// 		permissao();
																
												// 		// dasabilita opção de Check List no menu lateral
												// 	    //$scope.ver = 'Check-Out';
												// 	    //updateList();
				
												// 	    // atualiza verificação do Check List
												// 		//Check.clearCheckList();
												// 		$scope.statusMarcacao = 2; //efetuar a Saída
														
												// 		//salvando no BD SQLite
												// 		DB.selectMarcacao(Check.id).then(function(data){
												// 			if(data.rows.length){
												// 				DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
												// 					// console.log(data);
												// 				}, function(err){
												// 					console.log(err);
												// 				});
												// 			}else{
												// 				DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
												// 					// console.log(data);
												// 				}, function(err){
												// 					console.log(err);
												// 				});
												// 			}
												// 		}, function(err){
												// 			console.log(err);
												// 		});
																
												// 		// if(Check.cargo == "Supervisor Regional" || Check.cargo == "Supervisor Geral" || Check.cargo == "SUPERVISOR REGIONAL" || Check.cargo == "SUPERVISOR GERAL"){
												// 		if($scope.checkOutOk == 1){	
												// 			//chamar função para realizar Check List
												// 			pCheckList();
												// 		}
														
												// 		// exibe um alert para informar que o Check Out foi realizado com sucesso
												//         Popup.checkoutEfetuado();
				
												// 		// if($scope.lat2 == null && $scope.longt2 == null){
												// 		// 	// notifica que não foi possível encontrar a localização do dispositivo
												// 		// 	Popup.conexaoGPS();
												// 		// }
				
												// 		//enviando email para o funcionário
												// 		var data = {"loja": $scope.lojaOut, "checkout": $scope.currentOut, "funcionario": Check.id};
												//         WebService.sendEmail(data).success(function(data){
												//            	//alert(data);
												// 			console.log('E-mail de CheckOut enviado!');
												//         });
												//     }else {
												//        	// exibe que não foi possível enviar os dados para o banco do servidor 
												//         // console.log(data.result);
												// 		Popup.enviarCheckOutFail();
				
												// 		var mensagem = 'Erro ao salvar Check Out Online';
												// 		var dataHora = $scope.currentOut;
												// 		var erro = data.result;
												// 		console.log(mensagem, erro, dataHora);
				
												// 		DB.saveDebug(mensagem, erro, dataHora).then(function(data){
												// 			console.log('Debug Check Out Online salvo!');
												// 		}, function(err){
												// 			console.log('Erro ao salvar Debug Check Out Online!', err);
												// 		});
												//     }	            
												// });
												var currentTimeIn = null;
												//salvando no BD SQLite
												DB.saveCheckOut2(Check.id, currentTimeIn, $scope.currentOut, $scope.lojaOut, Localizacao.lat, Localizacao.longt, status,$scope.notacoes).then(function(data){
													//desativando div "enviando"
													$scope.send = false;
													console.log('Check Out Salvo');
													//armazena nome da Loja escaneada
													Check.pontocol2 = $scope.lojaOut;
													Check.currentTimeOut = $scope.currentOut;
				
													//verifar modo de envio
													Check.statusModeOut = status;
				
													// atualiza o ícone para Check Out nas telas
													Check.verific = 2;
													$scope.check = Check.verific;
													Check.checkout();
													notacoes = [];
													permissao();
															
													// dasabilita opção de Check List no menu lateral
													//$scope.ver = 'Check-Out';
													//updateList();
				
													// atualiza verificação do Check List
													//Check.clearCheckList();
													$scope.statusMarcacao = 2; //efetuar a Saída
													$scope.saida = 0;
													$scope.entrada = 1;
													
													//salvando no BD SQLite
													DB.selectMarcacao(Check.id).then(function(data){
														if(data.rows.length){
															DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}else{
															DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}
													}, function(err){
														console.log(err);
													});
															
													// if(Check.cargo == "Supervisor Regional" || Check.cargo == "Supervisor Geral" || Check.cargo == "SUPERVISOR REGIONAL" || Check.cargo == "SUPERVISOR GERAL"){
													if($scope.checkOutOk == 1){	
														//chamar função para realizar Check List
														pCheckList();
													}
													
													// exibe um alert para informar que o Check Out foi realizado com sucesso
													Popup.checkoutEfetuado();
				
													// if($scope.lat2 == null && $scope.longt2 == null){
													// 	// notifica que não foi possível encontrar a localização do dispositivo
													// 	Popup.conexaoGPS();
													// }
												}, function(err){
													//desativando div "enviando"
													$scope.send = false;
													   // exibe que não foi possível enviar os dados para o banco do servidor 
													// console.log(data.result);
													Popup.enviarCheckOutFail();
				
													var mensagem = 'Erro ao salvar Check Out Online';
													var dataHora = $scope.currentOut;
													var erro = data.result;
													console.log(mensagem, erro, dataHora);
				
													DB.saveDebug(mensagem, erro, dataHora).then(function(data){
														console.log('Debug Check Out Online salvo!');
													}, function(err){
														console.log('Erro ao salvar Debug Check Out Online!', err);
													});
												});
												User.send(); //Envia os dados para o servidor
											}else if(Network.status == false){
												//ativando div "enviando"
												$scope.send = true;
												//definindo conexão com internet como Offline
												var status = 'Offline';
												$scope.status = status;
												var cartao_pontocol = $scope.lojaOut;
												var currentTimeOut = $scope.currentOut;
												var currentTimeIn = null, latitude = $scope.lat2, longitude = $scope.longt2;
				
												console.log(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, latitude, longitude, status, $scope.notacoes);
												//salvando no BD SQLite
												DB.saveCheckOut2(funcionario, currentTimeIn, currentTimeOut, cartao_pontocol, Localizacao.lat, Localizacao.longt, status, $scope.notacoes).then(function(data){
													//desativando div "enviando"
													$scope.send = false;
													console.log('Check Out Salvo');
													
													//verifar modo de envio
													Check.statusModeOut = $scope.status;
				
													//armazena o nome da loja escaneada
													Check.pontocol2 = $scope.lojaOut;
													Check.currentTimeOut = $scope.currentOut;
														
													// atualiza o ícone para Check Out nas telas
													Check.verific = 2;
													Check.checkout();
													notacoes = [];
				
													// if($scope.lat2 == null && $scope.longt2 == null){
													// 	// notifica que não foi possível encontrar a localização do dispositivo
													// 	Popup.conexaoGPS();
													// }
				
													$scope.statusMarcacao = 2; //efetuar a Saída
													$scope.saida = 0;
													$scope.entrada = 1;
													
													//salvando no BD SQLite
													DB.selectMarcacao(Check.id).then(function(data){
														if(data.rows.length){
															DB.updateMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}else{
															DB.checkMarcacao(Check.id, $scope.statusMarcacao).then(function(data){
																// console.log(data);
															}, function(err){
																console.log(err);
															});
														}
													}, function(err){
														console.log(err);
													});
														
													//chamar função para realizar Check List
													// pCheckList();
														
													// dasabilita opção de Check List no menu lateral
													//$scope.ver = 'Check-Out';
													//updateList();
				
													// atualiza verificação do Check List
													//Check.clearCheckList();

													if($scope.checkOutOk == 1){	
														//chamar função para realizar Check List
														pCheckList();
													}
				
													//exibe um alert para informar que o Check Out foi realizado com sucesso
													Popup.checkoutEfetuado();
													$scope.check = Check.verific;
												}, function(err){
													//desativando div "enviando"
													$scope.send = false;
													console.log(err);
													Popup.enviarCheckOutFail();
				
													var mensagem = 'Erro ao salvar Check Out Offline';
													var dataHora = $scope.currentOut;
													console.log(mensagem, err, dataHora);
				
													DB.saveDebug(mensagem, err, dataHora).then(function(data){
														console.log('Debug Check Out Offline salvo!');
													}, function(err){
														console.log('Erro ao salvar Debug Check Out Offline!', err);
													});
												});
				
												//armazenando informações para serem salvas no arquivo.json
												// var info = {
												// 	"latitude": null,
												// 	"longitude": null,
												// 	"cartao_pontocol": $scope.lojaOut,
												// 	"funcionario": Check.id,
												// 	"checkin": "",
												// 	"checkout": currentTimeOut,
												// 	"status" : status
												// }
				
												// //salva os doados no arquivo.json
												// User.save(info, function (data, err) {
												// 	//desativando div "enviando"
												// 	$scope.send = false;
												// 	// console.log(data, err);
												// 	if(err){
												// 		console.log(err);
												// 		Popup.enviarCheckOutFail();
												// 	}else{
												// 		//verifar modo de envio
												// 		Check.statusModeOut = status;
				
												// 		//armazena o nome da loja escaneada
												// 		Check.pontocol2 = $scope.lojaOut;
												// 		Check.currentTimeOut = currentTimeOut;
															
												// 		// atualiza o ícone para Check Out nas telas
												// 		Check.verific = 2;
												// 		Check.checkout();
															
												// 		//chamar função para realizar Check List
												// 		pCheckList();
															
												// 		// dasabilita opção de Check List no menu lateral
												// 		//$scope.ver = 'Check-Out';
												// 		//updateList();
				
												// 		// atualiza verificação do Check List
												// 		//Check.clearCheckList();
				
												// 		//exibe um alert para informar que o Check Out foi realizado com sucesso
												// 		Popup.checkoutEfetuado();
												// 		$scope.check = Check.verific;
												// 	}
												// })
											}else{
												//do nothing
											}
										}else if(!result.text){
											// alert("Não foi possível escanear, tente novamente!");
										}else{
											Popup.qrcodeInvalido();
										}
									}else{
										//do nothing
									}
								}else{
									// Popup.lojaInexistente();
									invalido = 1;
								}
							}

							if(invalido == 1){
								Popup.lojaInexistente();
							}
						}else{
							//do nothing
						}
					}, function(err){
						console.log(err);
					});
				},function (error) {
					// alert("O escaneamento falhou, tente novamente! " + error);
				},
				{
					"preferFrontCamera" : false, // iOS and Android
					"showFlipCameraButton" : false, // iOS and Android
					"prompt" : "Escaneie o QR Code da localidade", // supported on Android only
					"formats" : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
					"orientation" : "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
				});
				// configurações da tela do QRCode Scanner
   			})
		}

		// função para realizar Logout da aplicação
		$scope.exit = function exit(){
			Popup.logout();

			/*if (Check.verific != 1){
				Popup.logout(function(){
					Check.verific = 3;
				});
			}else
				Popup.efetuarCheckOut();*/
		}

		$scope.localizacao = function(){
			Localizacao.posicao();
			if(Network.status){
				$state.go('mapa');
			}
		}

		$scope.updateMap = function(){
			Localizacao.updateMap();
		}
	}



