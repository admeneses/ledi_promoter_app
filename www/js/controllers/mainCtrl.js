angular
	.module('starter')
	.controller('mainCtrl', mainCtrl)
	mainCtrl.$inject = ['$interval', '$scope', 'Network', 'Check', 'Localizacao', 'Popup', 'User', 'Message', '$timeout', '$ionicPopup'];

	function mainCtrl($interval, $scope, Network, Check, Localizacao, Popup, User, Message, $timeout, $ionicPopup){
		$scope.status  		 = {color: "black"};
		$scope.gpsColor		 = {color: "black"};
		$scope.verific  	 = {color: "black"};
		$scope.verificList 	 = {color: "black"};

		//verificação do status de conexão com internet (criadas em networkService.js) e definição da cor do ícone nas telas
		$scope.$on("network-info",function(event, response){
			if(response){
				$scope.status.color = "#3CB371";

				User.send(); //Envia os dados para o servidor
				//alert("on");
			}
			else{
				$scope.status.color = "black";
				//alert("off");				
			}
			$scope.$apply(); //verificacao de variaveis usando Apply (atualizacao das variaveis do angular)
		})

		//verificação do status de Check In (1), Check Out (2) ou nunhum - (criadas em checkService.js) e definição da cor do ícone nas telas
		$scope.$on("verificacao",function(event, response){
			if(response == 1){
				$scope.verific.color = "#3CB371";
				//alert("on");
			}
			else if (response == 2){
				$scope.verific.color = "#B22222";
				//alert("off");					
			} else{
				$scope.verific.color = "black";
			}
			$scope.$apply(); //verificacao de variaveis usando Apply (atualizacao das variaveis do angular)
		});

		//verificação do status de Check List
		$scope.$on("verificacaoList",function(event, response){
			if(response == 1){
				$scope.verificList.color = "#3CB371";
			}else{
				$scope.verificList.color = "black";
			}
			$scope.$apply(); //verificacao de variaveis usando Apply (atualizacao das variaveis do angular)
		});

		//verificação do status de GPS
		$scope.$on("gps",function(event, response){
			if(response == 1){
				$scope.gpsColor.color = "#3CB371";
			}else{
				$scope.gpsColor.color = "black";
			}
			$scope.$apply(); //verificacao de variaveis usando Apply (atualizacao das variaveis do angular)
		});

		$scope.init = function () {
			var loclGPS = $interval(function() {
				// console.log("Se passaram 1 minuto!");
				// Localizacao.posicao();
				document.addEventListener("deviceready", onDeviceReady, true);
				
				function onDeviceReady() {
				
					// gpsDetect = cordova.require('cordova/plugin/gpsDetectionPlugin');
				
					// var checkButton = document.getElementById("check");
					
					// checkButton.onclick = function() {
						gpsDetect.checkGPS(onGPSSuccess, onGPSError);
					// } 
					
					function onGPSSuccess(on) {
						if (on){
							// console.log("GPS is enabled");
							Localizacao.gps = 'ON';
							Check.gps = 1;
							Check.gpsOn();
						} 
						else{
							// console.log("GPS is disabled");
							Localizacao.gps = 'OFF';
							Check.gps = 2;
							Check.gpsOff();
							
							// $ionicPopup.show({
							// 	'title': 'GPS desativado. Deseja ativá-lo?',
							// 	'buttons':[
							// 		{
							// 			'text': 'Não',
							// 			'type': 'button-assertive',
							// 			'onTap': function(e){
							// 				//do nothing
							// 			}
							// 		},
							// 		{
							// 			'text': 'Sim',
							// 			'type': 'button-balanced',
							// 			'onTap': function(e){
							// 				gpsDetect.switchToLocationSettings(onSwitchToLocationSettingsSuccess, onSwitchToLocationSettingsError);
							// 				$interval.cancel(loclGPS);
							// 			}
							// 		}
							// 	]
							// })
						} 
					}
					
					function onGPSError(e) {
						alert("Error : "+e);
						Localizacao.gps = 'OFF';
					}
					
					// var openSettingsButton = document.getElementById("openSettings");
			
					// openSettingsButton.onclick = function() {
					// }
			
					function onSwitchToLocationSettingsSuccess() {
						Localizacao.posicao();
						Localizacao.gps = 'ON';
					}
			
					function onSwitchToLocationSettingsError(e) {
						// alert("Error : "+e);
						Localizacao.gps = 'OFF';
					}
				}
			}, 15000);

			var localMessage = $interval(function() {
				// console.log("Se passaram 1 minuto!");
				Message.request();
			}, 10000);
		};
	};

