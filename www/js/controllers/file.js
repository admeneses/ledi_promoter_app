(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.controller('fileCtrl', fileCtrl)
	    fileCtrl.$inject = ['$scope', '$fileLogger', '$timeout', 'DateHour'];
		
		function fileCtrl($scope, $fileLogger, $timeout, DateHour){
             $scope.testing = function() {
                $fileLogger.setStorageFilename('myLog.txt');

                $fileLogger.log('debug', 'message');
                $fileLogger.log('info', 'message');
                $fileLogger.log('warn', 'message');
                $fileLogger.log('error', 'message');

                $fileLogger.debug('message');
                $fileLogger.info('message');
                $fileLogger.warn('message');
                $fileLogger.error('message');

                $fileLogger.log('error', 'error message', { code: 1, meaning: 'general' });

                $fileLogger.log('info', 'message', 123, [1, 2, 3], { a: 1, b: '2' });

                $timeout(function(){
                    $fileLogger.getLogfile().then(function(l) {
                        console.log('Logfile content');
                        console.log(l);
                    });
                }, 1000);

                $timeout(function(){
                    $fileLogger.checkFile().then(function(d) {
                        console.log('Logfile data');
                        console.log(JSON.stringify(d));
                    });
                }, 2000);

                $timeout(function(){
                    $fileLogger.deleteLogfile().then(function() {
                        console.log('Logfile deleted');
                    });
                }, 3000);

            }

	    }
})(); //autodeclaração