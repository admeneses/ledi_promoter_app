angular
.module('starter')
.controller('desbloqueioCtrl', desbloqueioCtrl)
desbloqueioCtrl.$inject = ['$scope', 'WebService', 'Network', 'Popup', 'DateHour', 'Check'];

function desbloqueioCtrl($scope, WebService, Network, Popup, DateHour, Check){
    $scope.send = false;

    //função para solicitar desbloqueio
    $scope.desbloquear = function(username){
        if(Network.status){
            $scope.send = true;
            var currentTimeDesbloqueio = DateHour.checkDesbloqueio();
            console.log(Check.id, username, currentTimeDesbloqueio); 
            WebService.solicitarDesbloqueio(Check.id, username, currentTimeDesbloqueio).success(function(data) {
                $scope.send = false;
                console.log(data);
                if(data.result.data){
                    Popup.desbloqueioEfetuado();
                }else{
                    Popup.desbloqueioNaoEfetuado(); 
                }
            });
        }else{
            Popup.desbloqueioOffline();
        }
    }
}
