(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.factory('Network', Network);

		function Network($rootScope, $timeout){
			var network = {};
			var timeOfInterval;

			network.status = false;
			// as ações serão usadas no mainCtrl
			// retorna que o dispositivo possui conexão com internet
	  	    document.addEventListener('online', function () {
	  	    	$timeout.cancel(timeOfInterval);
				timeOfInterval = $timeout(function(){
					network.status = true;
					console.log('INTERNET ON');
					$rootScope.$broadcast("network-info", true);
				}, 2000)
	     	})

	  	    // retorna que o dispositivo não possui conexão com internet
	    	document.addEventListener('offline', function () {
	    		$timeout.cancel(timeOfInterval);
				timeOfInterval = $timeout(function(){
					network.status = false;
					console.log('INTERNET OFF');
	        		$rootScope.$broadcast("network-info", false);
				}, 2000)
	    	})
	    	return network;
	    }
})(); //autodeclaração