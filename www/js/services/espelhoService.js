(function(){
	'use strict';

	angular
		.module('starter')
		.factory('Espelho', Espelho)
		Espelho.$inject = ['Check', 'Utils'];

        function Espelho(Check, Utils){
            var espelho = {};
            espelho.formatTable = formatTable;
            espelho.arrangeRoutine = arrangeRoutine;

            function checkDates(d1, d2) {
                var date1, date2,
                    day1, day2,
                    month1, month2,
                    year1, year2,
                    time1, time2;

                date1 	= new Date(d1);
                date2 	= new Date(d2);
                day1  	= date1.getDay();
                day2  	= date2.getDay();
                month1	= date1.getMonth();
                month2	= date2.getMonth();
                year1	= date1.getYear();
                year2	= date2.getYear();
                time1	= date1.getTime();
                time2	= date2.getTime();
                if(day1 == day2 && month1 == month2 && year1 == year2 && time2 >= time1)
                    return true;

                return false
            }

            function formatTable (res) {
                var aux 	= res,
                    result  = [[], []],
                    info    = null,
                    value 	= null;
        
                for(var i = 0; i < aux.length; i++) {
                    var changed = false, value = null;
        
                    info = aux[i]; // Informação da vez
                    info.pos = aux.length - i;
        
                    if(info.checkin && !info.checkout) {
                        result[0].push(info);
        
                    } else if(info.checkout && !info.checkin) {
                        for(var j = 0; j < result[0].length; j++) {
                            if(info.funcionario === result[0][j].funcionario && info.loja === result[0][j].loja && !result[0][j].updated) {
        
                                if(checkDates(result[0][j].checkin, info.checkout)) {
                                    value = result[0][j]; // Pego o horário do Check-in que foi eliminado
                                    changed = true;
                                    result[0].splice(j, 1);
        
                                    for(var k = 0; k < result[0].length; k++){
                                        if(result[0][k].funcionario === info.funcionario &&
                                            info.loja === result[0][k].loja){
                                            result[0][k].updated = true;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
        
                        result[1].push(info);
                        if(changed){ 
                            result[1][result[1].length - 1].checkin 	  = value.checkin;
                            result[1][result[1].length - 1].statusCheckin = value.status;
                        }
                    }
                }
        
                var data = [], i;
        
                // Coloca todos os Checkins na primeira linha da matriz da variavel data
                for(i = 0; i < result[0].length; i++) {
                    data.push(result[0][i]);
                }
        
                // Coloca todos os Checkins na segunda linha da matriz da variavel data
                for(i = 0; i < result[1].length; i++) {
                    data.push(result[1][i]);
                }
        
                // Calcula o tempo total que o funcionário ficou na loja no caso da coluna i da matriz data ter ambos o checkin e checkout
                var total = [0, 0], invalid = 0;
        
                for(i = 0; i < data.length; i++) {
                    if(data[i].checkin && data[i].checkout){
                        data[i].diff 	= diffTimes(data[i].checkin, data[i].checkout);
                        data[i].invalid	= false;
                        total 			= sumTimes(data[i].diff[0], data[i].diff[1], total);
                    } else {
                        data[i].diff 	= null;
                        data[i].invalid = true;
                        invalid++;
                    }
                }
        
                data.sort( function (v1, v2) {
                    if(v1.pos < v2.pos)
                        return -1
        
                    if(v1.pos > v2.pos)
                        return 1
        
                    return 0;
                })
        
                return [data, total, invalid];
            }

            function PHPToglobalDateFormat(date) {
                if(!date) return null; // Se a data não existir, retorna null
                
                var newDate = date.replace(' ', 'T');
                return newDate +'Z';
            }

            // Diferência dois horários no formato "yyyy-mm-dd hh:mm:ss"
            function diffTimes(begin, end) {
                var date01 = new Date(PHPToglobalDateFormat(begin)),
                    date02 = new Date(PHPToglobalDateFormat(end));
                
                var hour = 1000*60*60; // Esse valor é o respectivo a uma hora convertido em segundos;

                var diff = (date02.getTime() - date01.getTime()) / hour;
                // Retorna [hora, minuto, segundo]
                return [Math.floor(diff), getMinutesOfDecimalHours(diff), getSecondsOfDecimalHours(diff)];
            }

            // Passa um horário no formato decimal (15,121 por exemplo), e obtem a quantidade de minutos dele
            function getMinutesOfDecimalHours(hours) {
                var minutes = Math.floor((Math.abs(hours) * 60) % 60);
                return minutes;
            }
            // Passa um horário no formato decimal (15,121 por exemplo), e obtem a quantidade de segundos dele
            function getSecondsOfDecimalHours(hours) {
                var seconds = Math.floor((Math.abs(hours) * 60 * 60) % 60);
                return seconds;
            }

            function sumTimes(hours, minutes, total) {
                if(!total) total = [0,0];
                
                total[0] += hours;
                total[1] += minutes;
                total[0] += Math.floor(total[1] / 60);
                total[1] = total[1] % 60;
            
                return total;
            }
        
            function sumTimesWithSeconds(v1, v2) {
                var total = [0, 0, 0];
                if(typeof v1 !== 'object') v1 = [0, 0, 0];
                if(typeof v1 !== 'object') v2 = [0, 0, 0];
        
                total = total.map( function(value, index) {
                    return value + v1[index] + v2[index];
                });
        
                total[0] += Math.floor(total[1]/60); // Divide os minutos por 60 e pega o valor inteiro da divisão para as horas
                total[1] = total[1] % 60; // O resto da divisão será a quantidade de minutos reais que sobraram
                total[1] += Math.floor(v2[2]/60); // Divide os segundos por 60 e pega o valor inteiro da divisão para os minutos
                total[2] = total[2] % 60;
        
                return total;
            }
        
            function compareTimes(t1, t2) {
                if(!t1 || !t2)
                    return new Error('Está faltando algum horário');
        
                var date01 = Date.parse('01/01/2011 '+t1),
                    date02 = Date.parse('01/01/2011 '+t2);
        
                if(date01 > date02)
                    return -1;
                if(date02 > date01)
                    return 1;
                if(date01 == date02)
                    return 0;
            }

            var counter = 0;
            // Organiza a rotina do promotor
            function arrangeRoutine(cartao_ponto, loja_funcionario, timeToLunch) {
                console.log(cartao_ponto);
                // Filtra para trazer apenas os registros que estão completos
                var cartao_ponto_filtered = cartao_ponto.filter(function(ponto) {
                    // Se quiser só utilizar a loja que o funcionário pertence (&& ponto.loja.endsWith(loja_funcionario))
                    if(ponto.checkin && ponto.checkout)
                        return true;
        
                    return false;
                });

                // Mapeia os objetos para trazer apenas as informações necessárias
                var cartao_ponto_mapped = cartao_ponto_filtered.map( function(ponto) {
                    return {
                        checkin: ponto.checkin,
                        checkout: ponto.checkout,
                        diff: ponto.diff,
                        loja: ponto.loja,
                        status: ponto.status,
                        statusCheckin: ponto.statusCheckin,
                        timeToLunch: timeToLunch
                    }
                });
        
                cartao_ponto_mapped.reverse(); // Reverte o array, pois a ordem das datas está do último dia até o primeiro
                
                counter = 0;
                // Faz um reduce para organizar as batidas do cartão de ponto em uma fileira com 4 colunas
                var cartao_ponto_reduced = cartao_ponto_mapped.reduce(reduceCartaoPonto, []);
        
                var i, j, len, date, cartao, anterior, atual;
                for(i = 1, len = cartao_ponto_reduced.length; i < len; i++) {
                    anterior = new Date(PHPToglobalDateFormat(cartao_ponto_reduced[i - 1].cartao_ponto[0].ponto));
                    atual = new Date(PHPToglobalDateFormat(cartao_ponto_reduced[i].cartao_ponto[0].ponto));
        
                    var cartao_em_branco = [];
        
                    for(j = 1; j < Math.round(moment.duration(atual - anterior).asDays()); j++) {
                        date = moment(atual).subtract(j, 'days');
                        cartao_em_branco.push({
                            emBranco: true,
                            hasTimeToLunch: false,
                            timeToLunch: [0, 0, 0],
                            cartao_ponto: [],
                            diff: [0, 0, 0],
                            date: date.format('MM-DD'),
                            loja: '-- --'
                        })
                    }
                    
                    if(cartao_em_branco.length) {
                        cartao_em_branco.reverse(); // Deixa na ordem do cartao_ponto_reduced
                        cartao_ponto_reduced.splice(i, 0, ...cartao_em_branco);
                        i  	+= cartao_em_branco.length;
                        len = cartao_ponto_reduced.length;
                    }
                }
                
                cartao_ponto_reduced.reverse(); // Restaura as datas a posição atual
        
                return cartao_ponto_reduced;
            }

            function reduceCartaoPonto(state, ponto) {
                var newState = Utils.clone(state); // Clona o estado anterior
                // console.log('state', state);
                // console.log('newState', newState);
                // Em caso de lojas diferentes, pula para a próxima
                if(newState[counter] !== undefined) {
                    if(newState[counter].cartao_ponto !== undefined) {
                        if(newState[counter].cartao_ponto[0].loja !== ponto.loja) {
                            counter++;
                        } 
                        else if(diffTimes(newState[counter].cartao_ponto[0].ponto, ponto.checkout)[0] > 9)
                            counter++;
                    }
                }
        
                newState[counter] = newState[counter] || {cartao_ponto: [], diff: []};
        
                // Passa o registro de entrada
                newState[counter].cartao_ponto.push({ ponto: ponto.checkin, status: ponto.statusCheckin, loja: ponto.loja });
                // Passa o registro de saída
                newState[counter].cartao_ponto.push({ ponto: ponto.checkout, status: ponto.status, loja: ponto.loja });
        
                // Quando completar os 4 registro da linha da tabela, pula para a próxima
                if(newState[counter].cartao_ponto.length == 4){
                    var rotina = newState[counter].cartao_ponto;
                    console.log('rotina', rotina, newState[counter].diff, ponto.diff);
                    newState[counter].diff 		   = sumTimesWithSeconds(newState[counter].diff, ponto.diff); // Calcúla a diferença da primeira entrada com a última saída
                    newState[counter].timeToLunch  = diffTimes(rotina[1].ponto, rotina[2].ponto); // Calcula o tempo de almoço
                    if(ponto.timeToLunch !== null) {
                        newState[counter].invalidLunch = 
                            compareTimes(ponto.timeToLunch, newState[counter].timeToLunch.join(':')); // Verifica se o tempo de almoço feito foi válido
                    }
        
                    counter++;
                } else {
                    newState[counter].hasTimeToLunch = (ponto.timeToLunch !== null) ? true : false;
                    newState[counter].diff = ponto.diff; // Obtem a diferença de tempo dos registros		
                }
                
                return newState; // Retorna o estado atual
            }

            return espelho;
        }
})();