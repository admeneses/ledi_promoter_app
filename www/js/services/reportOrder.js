(function(){
	'use strict';

	angular
		.module('starter')
		.factory('Order', Order)
		Order.$inject = ['Check'];

        function Order(Check){
            var order = {};
            order.format = format;

            function checkDates(d1, d2) {
                var date1, date2,
                    day1, day2,
                    month1, month2,
                    year1, year2,
                    time1, time2;

                date1 	= new Date(d1);
                date2 	= new Date(d2);
                day1  	= date1.getDay();
                day2  	= date2.getDay();
                month1	= date1.getMonth();
                month2	= date2.getMonth();
                year1	= date1.getYear();
                year2	= date2.getYear();
                time1	= date1.getTime();
                time2	= date2.getTime();
                if(day1 == day2 && month1 == month2 && year1 == year2 && time2 >= time1)
                    return true;

                return false
            }

            function format(res){
                var aux 	= res,
                result  = [[], []],
                info    = null,
                value 	= null;

                for(var i = 0; i < aux.length; i++) {
                    var changed = false, value = null;

                    info = aux[i]; // Informação da vez
                    info.pos = aux.length - i;

                    if(info.checkin && !info.checkout) {
                        result[0].push(info);

                    } else if(info.checkout && !info.checkin) {
                        for(var j = 0; j < result[0].length; j++) {
                            if(info.funcionario === result[0][j].funcionario && info.loja === result[0][j].loja && !result[0][j].updated) {

                                if(checkDates(result[0][j].checkin, info.checkout)) {
                                    value = result[0][j].checkin; // Pego o horário do Check-in que foi eliminado
                                    changed = true;
                                    result[0].splice(j, 1);

                                    for(var k = 0; k < result[0].length; k++){
                                        if(result[0][k].funcionario === info.funcionario &&
                                            info.loja === result[0][k].loja){
                                            result[0][k].updated = true;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        result[1].push(info);
                        if(changed)
                            result[1][result[1].length - 1].checkin = value;
                    }
                }

                var data = [];

                for(var i = 0; i < result[0].length; i++) {
                    data.push(result[0][i]);
                }

                for(var i = 0; i < result[1].length; i++) {
                    data.push(result[1][i]);
                }

                data.sort( function (v1, v2) {
                    if(v1.pos < v2.pos)
                        return -1

                    if(v1.pos > v2.pos)
                        return 1

                    return 0;
                })

                return data;
            }

            return order;
        }
})();