(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.factory('Utils', Utils);
		Utils.$inject = ['$rootScope'];
		
		function Utils($rootScope){
            var utils = {};
            
            utils.copyArray 		= copyArray; // Copy a new Array
            utils.copyObject        = copyObject; // Copy a new Array
            utils.clone             = clone; // Clone aan object
            utils.convertToJSON 	= convertToJSON; // Convert an object to JSON
            utils.transformPHPDate 	= transformPHPDate; // Transform dd/mm/yyyy ----> yyyy-mm-dd
        
            // Other functions
            function copyArray(items) {
                var newArray = [];
        
                if(items)
                    items.forEach( function (item) {
                        newArray.push(item);
                    })
        
                return newArray;
            }
            function copyObject(object) {
                var newObject = {};
                
                for(var key in object) {
                    if(!object.hasOwnProperty(key)) return;
        
                    newObject[key] = object[key]
                }
        
                return newObject;
            }
            function clone(obj) {
                var copy;
        
                // Handle the 3 simple types, and null or undefined
                if (null == obj || "object" != typeof obj) return obj;
        
                // Handle Date
                if (obj instanceof Date) {
                    copy = new Date();
                    copy.setTime(obj.getTime());
                    return copy;
                }
        
                // Handle Array
                if (obj instanceof Array) {
                    copy = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        copy[i] = clone(obj[i]);
                    }
                    return copy;
                }
        
                // Handle Object
                if (obj instanceof Object) {
                    copy = {};
                    for (var attr in obj) {
                        if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
                    }
                    return copy;
                }
        
                throw new Error("Unable to copy obj! Its type isn't supported.");
            }
            function convertToJSON(value) {
                try {
                    return JSON.parse(value);
                } catch (err) {
                    return {};
                }
            }
            function transformPHPDate(date) {
                try {
                    date = date.split('/');
                    return date = date[2] +'-'+ date[1] +'-'+ date[0];
                } catch (err) {
                    console.log(err);
                    return null;
                }
            }
        
            return utils;
	    }
})(); //autodeclaração