(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.factory('User', User);
		User.$inject = ['FileSystem', 'WebService', 'Popup','Check', 'Network', 'DB', 'ChecklistService'];
		
		function User(FileSystem, WebService, Popup, Check, Network, DB, ChecklistService){
			var user = {};
			var File = new FileSystem.File();
			var count = 0;
			var total = 0;

			// array com as informações
			function setInfo(data) {
				try {
					user.info = JSON.parse(data);
				} catch (err) {
					user.info = [];
				}
			}

			function removeRegister(success) {
				// console.log(count, user.info, success);
				if(success) {
					user.info.splice(count, 1);
				}
				count--;
				if(count == -1) {
					removeInfo(user.info);
				}
			}

			// removendo informação por vez
			function removeInfo(info) {
				// console.log('informações', info);
				File.writeFile(info, function (data, err) {
					// console.log('Todos os registros foram enviados e apagados do arquivo!', data, err); //ERRO AQUI
					
					if(err){
						console.log("Não foi possível apagar os existentes");
						return;
					}

					total = 0;
					count = 0;
				})
			}

			// Variaveis do service "User"
			user.info = [];

			// Interfaces do service "User"
			user.getInfo  	= getInfo;
			user.removeAll 	= removeAll;
			user.removeInfo = removeInfo;
			user.save 	  	= save;
			user.send 	  	= send;
			user.dados	  	= dados;
			user.dadosSQLite = dadosSQLite;
			user.debug		 = debug;

			// selecionando o arquivo
			function getInfo(callback) {
				File.readFile( function (data, err) {
					if(err){
						console.log(err);
						
						if(callback)
							callback();

						return;
					}

					setInfo(data);
					//console.log('Selecionou o arquivo');
					if(callback)
						callback();
				})
			}

			// removendo todas as informações
			function removeAll(callback) {
				var data = [];

				File.writeFile(data, function (data, err) {
					if(err){
						callback(null, err);
						return;
					}

					setInfo(data);
					callback(user.info, null);
				})
			}

			// salvando informações
			function save(data, callback) {
				user.info.push(data);

				File.writeFile(user.info, function (data, err) {
					if(err){
						callback(null, err);
						return;
					}

					setInfo(data);
					//console.log('Salvou os registros!');
					callback(user.info, null);
				})
			}

			function debug(){
				DB.selectDebug().then(function(data){
					if(data.rows.length){
						//alert(data.rows);
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
							
							alert("Erro: \n" + values.mensagem + ", " + values.erro + ", " + values.dataHora);
						}
					}else{
						alert("Nenhuma informação de erro no dispositivo!");
					}
				}, function(err){
					console.log(err);
				});
			}

			function dados(){
				DB.selectLogin().then(function(data){
					if(data.rows.length){
						//alert(data.rows);
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
							
							alert("Login \n" + values.username + ", " + values.dataHora + ", " + values.status + ", " + values.versao);
						}
					}else{
						alert("Nenhum registro no dispositivo!");
					}
				}, function(err){
					console.log(err);
				});

				// var val = user.info;
				// if(val.length == 0){
				// 	//alert("Nenhum registro no arquivo!");
				// }else{
				// 	for(var i = 0; i < val.length; i++){
				// 		// if (val[i].checkin){
				// 		// 	alert("Entrada \nfuncionário: " + val[i].funcionario + ", " + val[i].checkin + ", " + val[i].cartao_pontocol + ", Latidude: " + val[i].latitude + ", Longitude: " + val[i].longitude + ", " + val[i].status);
				// 		// } else if (val[i].checkout) {
				// 		// 	alert("Saída \nfuncionário: " + val[i].funcionario + ", " + val[i].checkout + ", " + val[i].cartao_pontocol + ", Latidude: " + val[i].latitude + ", Longitude: " + val[i].longitude + ", " + val[i].status);
				// 		// }

				// 		if(val[i].questoes){
				// 			var aux = "";
				// 			for(var j = 0; j < val[i].questoes.length; j++){
				// 				aux += val[i].questoes[j].answer.choice + ", ";
				// 			}
				// 			alert("CheckList \nfuncionário: " + val[i].funcionario + ", " + val[i].loja + ", " + val[i].checklist + ", questões: " + aux + "questão 6 texto: " + val[i].questao6Text + ", Questão 11 texto: " + val[i].questao11Text + ", " + val[i].status);
				// 		}

				// 		// if(val[i].username){
				// 		// 	alert("Login \n" + val[i].username + ", " + val[i].currentTimeLogin + ", " + val[i].status + ", " + val[i].versao);
				// 		// 	// val[i].password + ", " +
				// 		// }	

				// 	}
				// }
			}

			function dadosSQLite(){
				DB.selectCheck2().then(function(data){
					if(data.rows.length){
						//alert(data.rows);
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
							if (values.checkin !== 'null'){
								alert("Entrada \n" + values.funcionario + ", " + values.checkin + ", " + values.cartao_pontocol + ", " + values.status);
							}else{
								alert("Saída \n" + values.funcionario + ", " + values.checkout + ", " + values.cartao_pontocol + ", " + values.status);
							}
						}	
					}else{
						alert("Nenhum registro no dispositivo!");
					}
				}, function(err){
					console.log(err);
				});
			}

			function apagarCheck(id){
				DB.deleteCheck2(id).then(function(data){
					console.log('Registro deletado', id);
				}, function(err){
					console.log(err);
				});

				DB.deleteCheck(id).then(function(data){
					console.log('Registro deletado', id);
				}, function(err){
					console.log(err);
				});
			}

			function apagarCheckList(id){
				DB.deleteCheckListDinamicoResposta(id).then(function(data){
					console.log('CheckList deletado', id);
				}, function(err){
					console.log(err);
				});
			}

			function apagarLogin(id){
				DB.deleteLogin(id).then(function(data){
					console.log('Login deletado', id);
				}, function(err){
					console.log(err);
				});
			}

			// enviando informações para os webservices
			function send() {
				// getInfo(function () {
					
				// 	var val = user.info.map(function(e){
				// 		return e;
				// 	}); 

				// 	total = user.info.length;
				// 	count = val.length - 1;
				// 	val.reverse();
				// 	console.log("--- Inicializando envio arquivo... ---");
				// 	for(var i = val.length - 1; i >= 0; i--) {
				// 		// console.log(val[i], 'VAL');

				// 		// // informações de check in
				// 		// if (val[i].checkin){
				// 		// 	WebService.doCheckIn(val[i].funcionario, val[i].checkin, val[i].cartao_pontocol, val[i].latitude, val[i].longitude, val[i].status).success(function(res) {
        		// 		// 	    if(res.result == 'Sucesso') {
				// 		//            	//console.log(res.info);
				// 		//            	var data = {"funcionario": res.info[':funcionario'], "loja": res.info[':cartao_pontocol'], "checkin": res.info[':checkin']};
				// 		// 			WebService.sendEmail(data).success(function(data){
				// 		// 	           	console.log(data);
				// 		// 	        }).error(function(data){
				// 		// 				console.log(data);
				// 		// 			});

				// 		// 			removeRegister(true);
				// 		//         }else {
				// 		// 			removeRegister(false);
				// 		//            	// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		//             //alert(res.result);
				// 		//         }	            
				// 		// 	});
				// 		// 	// informações de check out
				// 		// } else if (val[i].checkout) {
				// 		// 	WebService.doCheckOut(val[i].funcionario, val[i].checkout, val[i].cartao_pontocol, val[i].latitude, val[i].longitude, val[i].status).success(function(res) {    
				// 		// 		if(res.result == 'Sucesso') {						            
				// 		//          //console.log(res.info);
				// 		//          var data = {"funcionario": res.info[':funcionario'], "loja": res.info[':cartao_pontocol'], "checkout": res.info[':checkout']};
				// 		// 			WebService.sendEmail(data).success(function(data){
				// 		// 	           	console.log(data);
				// 		// 	        }).error(function(data){
				// 		// 				console.log(data);
				// 		// 			});

				// 		// 			removeRegister(true);
				// 		//         }else {
				// 		// 			removeRegister(false);
				// 		//           	// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		//             //alert(res.result);
				// 		// 	    }	            
				// 		// 	});
				// 		// }

				// 		// informações de check list
				// 		if(val[i].questoes){
				// 			console.log('CheckList', val[i].questoes);
				// 			WebService.doCheckList(val[i].questoes, val[i].funcionario, val[i].loja, val[i].checklist, val[i].questao6Text, val[i].questao11Text, val[i].status).success(function(res) {    
				// 				//console.log(res);
				// 				if(res.result == 'Sucesso') {
				// 					//Check.checklist();
				// 					//console.log(res);
				// 					removeRegister(true);									
				// 		        }else {
				// 					removeRegister(false);
				// 		          	// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		            //Popup.enviarCheckListFail();
				// 			    }	            
				// 			});
				// 		}

				// 		// informações de login
				// 		// if(val[i].username){
				// 		// 	//console.log(val[i].username);
				// 		// 	WebService.doLogin(val[i].username, val[i].password, val[i].currentTimeLogin, val[i].status, val[i].versao).success(function(res) {    
				// 		// 		//console.log(res);
				// 		// 		//res.result == 'Sucesso'
				// 		// 		if(res.result.tblogin == 'Sucesso') {
				// 		// 			//console.log(res);
				// 		// 			removeRegister(true);
				// 		//         }else {
				// 		// 			removeRegister(false);
				// 		//           	// exibe que não foi possível enviar os dados para o banco do servidor 
				// 		//             //alert(res.result);
				// 		// 	    }	            
				// 		// 	});
				// 		// }
				// 	}
				// 	console.log("--- Fim do envio arquivo ---");
				// });
				sendCheck();
				sendLogin();
				sendCheckList();
			}
			
			function sendCheck(){
				// console.log('entrou');
				DB.selectCheck2().then(function(data){
					if(data.rows.length){
						// console.log(data.rows);
						var pos = 0;
						sendingCheck(pos, data);
					}
				}, function(err){
					alert(err);
				});

				DB.selectCheck().then(function(data){
					if(data.rows.length){
						// console.log(data.rows);
						var pos = 0;
						sendingCheck(pos, data);
					}
				}, function(err){
					alert(err);
				});
			}

			function sendingCheck(pos, checks){
				console.log('entrou no sending', checks.rows.length, pos);
				if(pos <= checks.rows.length-1){
					var value = checks.rows.item(pos);
					console.log(value);
					if(value.checkin !== 'null'){
						(function (value){
							console.log('CheckIn', value);
							WebService.doCheckIn(value.funcionario, value.checkin, value.cartao_pontocol, value.latitude, value.longitude, value.status, value.notacoes).success(function(res) {
								console.log(res);
								if(res.result == 'Sucesso') {
									//console.log(res.info);
									var data = {"funcionario": value.funcionario, "loja": value.cartao_pontocol, "checkin": value.checkin};
									WebService.sendEmail(data).success(function(data){
										// console.log(data);
										console.log('E-mail de CheckIn enviado!');
									}).error(function(data){
										console.log(data);
									});

									//deletar do SQLite
									var id = value.id;
									apagarCheck(id);
								}else if(res.result == 'Duplicado'){
									console.log('Registro duplicado');
									//deletar do SQLite
									var id = value.id;
									apagarCheck(id);
								}else {
									console.log(res.info);
								}

								pos++;
								sendingCheck(pos, checks);	            
							});
						})(value);
					}else{
						(function (value){
							console.log('CheckOut', value);
							WebService.doCheckOut(value.funcionario, value.checkout, value.cartao_pontocol, value.latitude, value.longitude, value.status, value.notacoes).success(function(res) {    
								if(res.result == 'Sucesso') {						            
									console.log(res.info);
									var data = {"funcionario": value.funcionario, "loja": value.cartao_pontocol, "checkout": value.checkout};
									WebService.sendEmail(data).success(function(data){
										// console.log(data);
										console.log('E-mail de CheckOut enviado!');
									}).error(function(data){
										console.log(data);
									});

									//deletar do SQLite
									var id = value.id;
									apagarCheck(id);
								}else if(res.result == 'Duplicado'){
									console.log('Registro duplicado');
									//deletar do SQLite
									var id = value.id;
									apagarCheck(id);
								}else{
									console.log(res.info);
								}
								
								pos++;
								sendingCheck(pos, checks);	 	            
							});
						})(value);
					}
				}else{
					//do nothing
				}
			}

			//Funções para Login SQLite
			function sendLogin(){
				// console.log('entrou');
				DB.selectLogin().then(function(data){
					if(data.rows.length){
						// console.log(data.rows);
						var pos = 0;
						sendingLogin(pos, data);
					}
				}, function(err){
					alert(err);
				});
			}

			function sendingLogin(pos, checks){
				// console.log('entrou no sendingLogin', checks.rows.length, pos);
				if(pos <= checks.rows.length-1){
					var value = checks.rows.item(pos);
					//console.log(value);
					if(value.username){
						(function (value){
							console.log('Login', value);
							WebService.doLogin(value.username, value.password, value.dataHora, value.status, value.versao, value.uuid, value.model, value.versionDevice).success(function(res) {
								console.log(res);
								// console.log(res.result.tbfuncionario);
								if(res.result.tblogin == 'Sucesso') {
									//deletar do SQLite
									var id = value.id;
									apagarLogin(id);
								}else {
									//console.log(res.info);
								}

								pos++;
								sendingLogin(pos, checks);	            
							});
						})(value);
					}
				}else{
					//do nothing
				}
			}

			//Funções para Login SQLite
			function sendCheckList(){
				// console.log('entrou');
				DB.selectCheckListDinamicoResposta().then(function(data){
					if(data.rows.length){
						// console.log(data.rows);
						var pos = 0;
						sendingCheckList(pos, data);
					}
				}, function(err){
					alert(err);
				});
			}

			function sendingCheckList(pos, checks){
				// console.log('entrou no sendingLogin', checks.rows.length, pos);
				if(pos <= checks.rows.length-1){
					var value = checks.rows.item(pos);
					//console.log(value);
					if(value.checklist){
						(function (value){
							// console.log('CheckList', value.checklist);
							var checklist = JSON.parse(value.checklist);
							
							// console.log('CheckList', checklist);
							
							var send_checklist = {
								checklist: checklist.info,
								questions: checklist.questions
							};
		
							send_checklist.questions = send_checklist.questions.map(function(question){
								if(typeof question.answer == "object"){
									question.answer = question.answer.title;
								}
								return question;
							});
							
							// console.log(send_checklist);
							ChecklistService.sendChecklist(send_checklist, value.funcionario, value.dataHora, value.loja)
								.then( function(res) {
									// $scope.send = false;
									console.log(res);
									// exibindo PopUp de check list enviado
									// Popup.enviarCheckList();
									// atualizando cor de check list enviado
									// Check.checklist();
									// redireciona para a tela principal do app
									// $state.go('presenca');
									var id = value.id;
									apagarCheckList(id);
								})
								.catch( function(err) {
									// $scope.send = false;
									console.log(err);
									// exibe um PopUp sobre o envio do check list ter falhado
									// Popup.enviarCheckListFail();
									// $scope.verificList = 2;
								})
							// return WebService.enviarCheckList(value.checklist, value.funcionario, value.datahora, value.ambiente);  
							// WebService.enviarCheckList(value.checklist, value.funcionario, value.datahora, value.ambiente).success(function(res) {
							// 	console.log(res);
							// 	// console.log(res.result.tbfuncionario);
							// 	if(res.message == 'Respostas salvas com sucesso') {
									//deletar do SQLite
									// var id = value.id;
									// apagarCheckList(id);
							// 	}else {
							// 		//console.log(res.info);
							// 	}

								pos++;
								sendingCheckList(pos, checks);	            
							// });
						})(value);
					}
				}else{
					//do nothing
				}
			}

			return user;
	    }
})(); //autodeclaração

