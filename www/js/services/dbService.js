(function(){
	'use strict';
		
	angular
		.module('starter')
		.factory('DB', DB)
		DB.$inject = ['SQLITE_CONFIG', '$q', '$cordovaSQLite', 'DateHour', 'Check'];

		function DB(SQLITE_CONFIG, $q, $cordovaSQLite, DateHour, Check){
			var db = {};

			db.initDb = initDb;
			db.connection = null;

			// iniciando bd
			function initDb(){
				document.addEventListener('deviceready', function (){
					db.connection = window.sqlitePlugin.openDatabase({
						'name' : SQLITE_CONFIG.name,
						'location' : SQLITE_CONFIG.location
					});

					defineTables();
				});

				//defineTables(null); //teste não tem conexão então passamos null
			};	

			//cria query das tabelas
			function defineTables(){
				/*console.log(SQLITE_CONFIG.tables);*/
				var tables = SQLITE_CONFIG.tables;
				var configTables, table, label, info;
				configTables = [];

				for (table in tables){
					var values = "";

					//retia qualquer código que não foi criado na etapa de configuração
					if(!tables.hasOwnProperty(table)) continue;
					
					//console.log(tables[table]); //acessando as propriedades (chaves e valores) das tabelas
					
					for(label in tables[table]){
						//console.log(label); // acessando as chaves da tabela
						if(label === 'options')
							values += tables[table][label] + ',';
						else 
							values += label + ' ' + tables[table][label] + ',';
					}
					//retirando vírgula do final
					values = values.substring (0, values.length - 1);
					configTables.push('CREATE TABLE IF NOT EXISTS ' + table + '( ' + values + ')');
				}
				createTables(configTables);
			}

			//cria tabelas
			function createTables(configTables){
				for(var i = 0; i < configTables.length; i++ ){
					//console.log(configTables[i]);
					var request = transaction(configTables[i]); 
					request.then(function(data){
						console.log(data);
					}, function(error){
						console.log(error);
					})
					
				}
			};

			//transacao - executa o código dentro do sql, poupando repetição de codigo (organização)
			function transaction(query){
				return $q( function (response, reject) {
					db.connection.transaction( function (tx) {
						tx.executeSql(query, null, function (tx, res) {
							response(res);
						}, function (tx, err) {
							reject(err);
						})
					})
				})
			};

			//atrelando uma funcao a bd, responposável por retornar alguma resultado para o usuario
			db.login = function(){
				var sql = "SELECT * FROM tbFuncionarios";
				//var sql = "SELECT id, nome, sobrenome, email, cargo FROM tbFuncionarios WHERE username = '" + username + "' AND senha =  '" + password + "'";									
				return transaction(sql);
			}

			//selecionando último funcionário adicionado 
			db.userSave = function(){
				var sql = "SELECT username, senha FROM tbFuncionarios ORDER BY id DESC LIMIT 1";									
				return transaction(sql);
			}

			//checando se o usuário existe no banco
			db.checkUser = function(funcionario, nome, sobrenome, email, username, senha, cargo, status, idChecklist, lojaFunc, lojaLat, lojaLong){
				var sql = "INSERT OR REPLACE INTO tbFuncionarios (funcionario, nome, sobrenome, email, username, senha, cargo, status, idChecklist, lojaFunc, lojaLat, lojaLong) VALUES('" + funcionario + "','" + nome + "','" +  sobrenome + "','" + email + "','" + username + "','" + senha + "','" + cargo + "','" + status + "','" + idChecklist + "','" + lojaFunc + "','" + lojaLat + "','" + lojaLong + "')";									
				return transaction (sql);
			}

			//checando se o usuário existe no banco para atualizar seus dados ou inserir dados caso não exista
			db.updateUser = function(funcionario, nome, sobrenome, email, username, senha, cargo, status, idChecklist, lojaFunc, lojaLat, lojaLong){
				var sql = "UPDATE tbFuncionarios SET nome = '" + nome + "', sobrenome = '" + sobrenome + "', email = '" + email + "', username = '" + username + "', senha = '" + senha + "', cargo = '" + cargo + "', status = '" + status + "', idChecklist = '" + idChecklist + "', lojaFunc = '" + lojaFunc + "', lojaLat = '" + lojaLat + "', lojaLong = '" + lojaLong + "' WHERE funcionario = '" + funcionario + "'";									
				return transaction (sql);
			}

			//atualizando senha
			db.updatePassword = function(funcionario, newPassword){
				var sql = "UPDATE tbFuncionarios SET senha = '" + newPassword + "' WHERE funcionario = '" + funcionario + "'";									
				return transaction (sql);
			}

			//selecionando a última mensagem da tabela marketing
			db.marketing = function(){
				var sql = "SELECT MAX(id), mensagem, created_at FROM tbMarketing GROUP BY id";
				return transaction(sql);
			}

			//inserindo uma nova mensagem e data/ hora na tabela marketing
			db.saveAvisos = function(id, mensagem, created_at){
				var sql = "INSERT OR REPLACE INTO tbMarketing (id, mensagem, created_at) VALUES('" + id + "','" +  mensagem + "','" + created_at + "')";									
				return transaction (sql);
			}

			//inserindo uma nova rota na tabela routes
			db.saveRoutes = function(id, nome, funcionario, dia_da_semana, data){
				var sql = "INSERT OR REPLACE INTO tbRoutes(id, nome, funcionario, dia_da_semana, data) VALUES('" + id + "','" +  nome + "','" + funcionario + "','" + dia_da_semana + "','" + data + "')";									
				return transaction (sql);
			}

			//selecionando rotas da tabela routes
			db.routes = function(){
				var sql = "SELECT * FROM tbRoutes WHERE funcionario = '" + Check.id + "'";
				return transaction(sql);
			}

			//apagando rotas da tabela routes
			db.dropRoutes = function(){
				var sql = "DELETE FROM tbRoutes WHERE funcionario = '" + Check.id + "'";
				return transaction(sql);
			}

			//inserindo checkin ou chekout na tabela cartao_ponto
			db.saveCheckIn = function(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status){
				var sql = "INSERT INTO tbCartao_ponto(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status) VALUES('" + funcionario + "','" + checkin + "','" + checkout + "','" + cartao_pontocol + "','" + latitude + "','" + longitude + "','" + status + "')";
				return transaction(sql);
			}

			//inserindo chekout na tabela cartao_ponto
			db.saveCheckOut = function(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status){
				var sql = "INSERT INTO tbCartao_ponto(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status) VALUES('" + funcionario + "','" + checkin + "','" + checkout + "','" + cartao_pontocol + "','" + latitude + "','" + longitude + "','" + status + "')";
				return transaction(sql);
			}

			//selecionando registros para exibição das Entradas e Saídas
			db.selectCheck = function(){
				var sql = "SELECT * FROM tbCartao_ponto ORDER BY id";
				return transaction(sql);
			}

			//apagando registro inserido na tabela cartao_ponto
			db.deleteCheck = function(id){
				var sql = "DELETE FROM tbCartao_ponto WHERE id = '" + id + "'";
				return transaction(sql);
			}

			//inserindo checkin ou chekout na tabela cartao_ponto2
			db.saveCheckIn2 = function(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status, notacoes){
				var sql = "INSERT INTO tbCartao_ponto2(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status, notacoes) VALUES('" + funcionario + "','" + checkin + "','" + checkout + "','" + cartao_pontocol + "','" + latitude + "','" + longitude + "','" + status + "','" + notacoes + "')";
				return transaction(sql);
			}

			//inserindo chekout na tabela cartao_ponto
			db.saveCheckOut2 = function(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status, notacoes){
				var sql = "INSERT INTO tbCartao_ponto2(funcionario, checkin, checkout, cartao_pontocol, latitude, longitude, status, notacoes) VALUES('" + funcionario + "','" + checkin + "','" + checkout + "','" + cartao_pontocol + "','" + latitude + "','" + longitude + "','" + status + "','" + notacoes + "')";
				return transaction(sql);
			}

			//selecionando registros para exibição das Entradas e Saídas
			db.selectCheck2 = function(){
				var sql = "SELECT * FROM tbCartao_ponto2 ORDER BY id";
				return transaction(sql);
			}

			//apagando registro inserido na tabela cartao_ponto
			db.deleteCheck2 = function(id){
				var sql = "DELETE FROM tbCartao_ponto2 WHERE id = '" + id + "'";
				return transaction(sql);
			}

			//inserindo dados de login na tabela login
			db.saveLogin= function(username, password, dataHora, status, versao, uuid, model, versionDevice){
				var sql = "INSERT INTO tbLogin(username, password, dataHora, status, versao, uuid, model, versionDevice) VALUES('" + username + "','" + password + "','" + dataHora + "','" + status + "','" + versao + "','" + uuid + "','" + model + "','" + versionDevice + "')";
				return transaction(sql);
			}

			//selecionando registros para exibição dos Logins
			db.selectLogin = function(){
				var sql = "SELECT * FROM tbLogin ORDER BY id";
				return transaction(sql);
			}

			//apagando registro inserido na tabela login
			db.deleteLogin = function(id){
				var sql = "DELETE FROM tbLogin WHERE id = '" + id + "'";
				return transaction(sql);
			}

			//inserindo dados de erros na tabela debug
			db.saveDebug = function(mensagem, erro, dataHora){
				var sql = "INSERT INTO tbDebug(mensagem, erro, dataHora) VALUES('" + mensagem + "','" + erro + "','" + dataHora + "')";
				return transaction(sql);
			}

			//selecionando registros para exibição dos erros
			db.selectDebug = function(){
				var sql = "SELECT * FROM tbDebug ORDER BY id";
				return transaction(sql);
			}

			//apagando registros inseridos na tabela debug
			db.deleteDebug = function(id){
				var sql = "DELETE FROM tbDebug";
				return transaction(sql);
			}

			//salvando status de marcação (Entrado ou Saída) do funcionário
			db.checkMarcacao = function(id, status){
				var sql = "INSERT OR REPLACE INTO tbMarcacao (id, status) VALUES('" + id + "','" + status + "')";									
				return transaction (sql);
			}

			//atualizando status de marcação (Entrado ou Saída) do funcionário
			db.updateMarcacao = function(id, status){
				var sql = "UPDATE tbMarcacao SET status = '" + status + "' WHERE id = '" + id + "'";									
				return transaction (sql);
			}

			//selecionando o status do funcionário
			db.selectMarcacao = function(id){
				var sql = "SELECT * FROM tbMarcacao WHERE id = '" + id + "' ORDER BY id DESC LIMIT 1";
				return transaction(sql);
			}

			//inserindo permissões na tabela de Configuração
			db.checkConfiguracao = function(id, gpsAtivo, metrosAtivo, cargoQuinhentos, lojasInseridas){
				var sql = "INSERT OR REPLACE INTO tbConfiguracao (id, gpsAtivo, metrosAtivo, cargoQuinhentos, lojasInseridas) VALUES('" + id + "','" + gpsAtivo + "','" + metrosAtivo + "','" + cargoQuinhentos + "','" + lojasInseridas + "')";									
				return transaction (sql);
			}

			db.selectConfiguracao = function(){
				var sql = "SELECT * FROM tbConfiguracao";
				return transaction(sql);
			}

			//inserindo novas permissões na tabela de AvisoBloqueio
			db.checkAvisoBloqueio = function(id, bloqueiahora, bloqueiagpsmock){
				var sql = "INSERT OR REPLACE INTO tbAvisoBloqueio (id, bloqueiahora, bloqueiagpsmock) VALUES('" + id + "','" + bloqueiahora + "','" + bloqueiagpsmock + "')";									
				return transaction (sql);
			}

			db.selectAvisoBloqueio = function(){
				var sql = "SELECT * FROM tbAvisoBloqueio";
				return transaction(sql);
			}

			//inserindo lojas na tabela de tbLoja
			db.checkLojas = function(id, codigo, nome, latitude, longitude){
				var sql = "INSERT OR REPLACE INTO tbLoja (id, codigo, nome, latitude, longitude) VALUES('" + id + "','" + codigo + "','" + nome + "','" + latitude + "','" + longitude + "')";									
				return transaction (sql);
			}

			db.selectLojas = function(){
				var sql = "SELECT * FROM tbLoja";
				return transaction(sql);
			}

			//inserindo permissoes das funcionalidades
			db.insertPermissao = function(funcionario, id, tipo, modulo){
				var sql = "INSERT INTO tbPermissao (funcionario, id, tipo, modulo) VALUES ('" + funcionario + "', '" + id + "', '" + tipo + "', '" + modulo + "')";
				return transaction(sql);
			}

			//selecionando permissoes por modulo
			db.selectPermissao = function(funcionario){
				var sql = "SELECT * FROM tbPermissao WHERE funcionario = '" + funcionario + "'";
				return transaction(sql);
			}

			//apagando permissoes
			db.deletePermissao = function(funcionario){
				var sql = "DELETE FROM tbPermissao WHERE funcionario = '" + funcionario + "'";
				return transaction(sql);
			}

			//inserindo lojas
			db.saveLojas = function(id, nome, codigo){
				var sql = "INSERT INTO tbLojas (id, nome, codigo) VALUES ('" + id + "', '" + nome + "', '" + codigo + "')";
				return transaction(sql);
			}

			//selecionando lojas
			db.selectLojas = function(){
				var sql = "SELECT * FROM tbLojas ORDER BY id";
				return transaction(sql);
			}

			//apagando lojas
			db.deleteLojas = function(){
				var sql = "DELETE FROM tbLojas";
				return transaction(sql);
			}

			//inserindo batida na tabela tbCheckListDinamico
			db.saveCheckListDinamico = function(id, funcionario, checklist){
				var sql = "INSERT INTO tbCheckListDinamico(idChecklist, funcionario, checklist) VALUES('" + id +  "','" + funcionario + "','" + checklist +  "')";
				return transaction(sql);
			}

			//selecionando registros para exibição do CheckListDinamico
			db.selectCheckListDinamico = function(){
				var sql = "SELECT * FROM tbCheckListDinamico WHERE funcionario = '" + Check.id + "'";
				return transaction(sql);
			}

			//apagando registro inserido na tabela tbCheckListDinamico
			db.deleteCheckListDinamico = function(){
				var sql = "DELETE FROM tbCheckListDinamico WHERE funcionario = '" + Check.id + "'";
				return transaction(sql);
			}

			//inserindo batida na tabela tbCheckListDinamicoResposta
			db.saveCheckListDinamicoResposta = function(funcionario, checklist, dataHora, loja){
				var sql = "INSERT INTO tbCheckListDinamicoResposta(funcionario, checklist, dataHora, loja) VALUES('" + funcionario + "','" + checklist + "','" + dataHora + "','" + loja +  "')";
				return transaction(sql);
			}

			//selecionando registros para exibição do CheckListDinamicoResposta
			db.selectCheckListDinamicoResposta = function(){
				var sql = "SELECT * FROM tbCheckListDinamicoResposta ORDER BY id";
				return transaction(sql);
			}

			//apagando registro inserido na tabela tbCheckListDinamicoResposta
			db.deleteCheckListDinamicoResposta = function(id){
				var sql = "DELETE FROM tbCheckListDinamicoResposta WHERE id = '" + id + "'";
				return transaction(sql);
			}

			return db;
		};

})();