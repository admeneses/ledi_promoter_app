(function() {
    'use strict';

    angular
        .module('starter')
        .factory('ChecklistService', checklistService);

        function checklistService($q, Check, WebService, DateHour, Popup, Network, DB) {
            var checklist = {};

            // Variables
            checklist.types = [
                { name: 'Resposta curta', hasOptions: false },
                { name: 'Resposta longa', hasOptions: false },
                { name: 'Escolha uma', hasOptions: true },
                { name: 'Múltipla escolha', hasOptions: true },
                { name: 'Seleção', hasOptions: true },
                { name: 'Number', hasOptions: false }
            ]

            // Actions
            checklist.getChecklist  = getChecklist;
            checklist.sendChecklist = sendChecklist;
            checklist.getChecklistSaveOffline = getChecklistSaveOffline;

            function getChecklist() {
                var response = { hasChecklist: false, checklist: null };

                return $q( function(resolve, reject) {
                    if(!Check.idChecklist || Check.idChecklist == 0)
                        return resolve(response);
                    // console.log('Service', Check.idChecklist);
                    WebService.receberCheckList(Check.idChecklist)
                        .then( function(res) {
                            // var checklist = JSON.stringify(res.data);
                            // console.log(checklist);
                            if(!res.data != 'null') {
                                response.hasChecklist = true;
                                response.checklist    = {
                                    info: res.data.checklist,
                                    questions: res.data.questions
                                }
                                
                                //Apagando dados da tabela checklist
                                DB.deleteCheckListDinamico().then(function(data){
                                    console.log(data);
                                    console.log('deletou');
                                }, function(err){
                                    console.log(err);
                                });

                                // Inserir no banco de dados
                                console.log(Check.idChecklist, Check.id, checklist);
                                // DB.saveCheckListDinamico(Check.idChecklist, Check.id, checklist).then(function(data){
                                //     console.log(data);
                                // }, function(err){
                                //     console.log(err);
                                // });

                                getChecklistSaveOffline();
                            }
                            resolve(response);
                        })
                        .catch( function(err) {
                            // alert(err);
                            resolve(response);
                        })
                })
            }

            function getChecklistSaveOffline() {
                // console.log(id);
                var response = { hasChecklist: false, checklist: null };

                return $q( function(resolve, reject) {
                    // if(!Check.idChecklist || Check.idChecklist == 0)
                    //     return resolve(response);
                    // console.log('Service', Check.idChecklist);
                    WebService.receberCheckList(Check.idChecklist)
                        .then( function(res) {
                            var checklist = JSON.stringify(res.data);
                            // console.log(checklist);
                            if(!res.data != 'null') {
                                response.hasChecklist = true;
                                response.checklist    = {
                                    info: res.data.checklist,
                                    questions: res.data.questions
                                }
                                console.log(res.data.checklist.id);
                                //Inserir no banco de dados
                                console.log(Check.idChecklist, Check.id, checklist);
                                DB.saveCheckListDinamico(Check.idChecklist, Check.id, checklist).then(function(data){
                                    console.log(data);
                                    console.log('salvou');
                                }, function(err){
                                    console.log(err);
                                });
                            }
                            resolve(response);
                        })
                        .catch( function(err) {
                            // alert(err);
                            resolve(response);
                        })
                })
            }

            // hora e data do check list
			// var currentTimeList = null;
            // var checkLoja = Check.pontocol2;

            function sendChecklist(checklist, funcionario, dataHora, checkLoja) {
                // currentTimeList = DateHour.checkList();

                // if(Network.status){
                    // return WebService.enviarCheckList(checklist, Check.id, currentTimeList, Check.ambiente);  
                    console.log(checklist);
                    return WebService.enviarCheckList(checklist, funcionario, dataHora, checkLoja);   
                // }else{
                //     // checklist = JSON.stringify(checklist);
                //     console.log(checklist, funcionario, dataHora, ambiente);
                //     return WebService.enviarCheckList(checklist, funcionario, dataHora, ambiente);   
                    
                // }
                // if(Check.pontocol2 == "Não realizado"){
                //     return WebService.enviarCheckList(checklist, Check.id, currentTimeList, Check.pontocol);                                        
                // }else{
                    // return WebService.enviarCheckList(checklist, Check.id, currentTimeList, Check.ambiente);                    
                // }
            }

            return checklist;
        }
})();