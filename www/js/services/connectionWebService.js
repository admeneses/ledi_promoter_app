(function(){
	'use strict';

	angular
		.module('starter')
		.factory('WebService', WebService)
		WebService.$inject = ['$http', '$window'];

	function WebService($http, $window){
		//variaveis URL para envio dos webservices

		// var url = 'http://promoter.ledicloud.com/webservices/';
		var url = 'http://promoter.ledicloud.com/test/webservices/';
		// var url = 'http://192.168.0.102:80/Projetos_LEDi/LEDi_PROMOTER/webservices/';

		
		//função para receber os dados de Login do banco de dados do servidor (POST)
		var _doLogin = function(username, password, dataHora, status, versao, uuid, model, versionDevice) {
    		return $http({
			   "url": url + 'login_4110.php',
			   "method": "POST",
			   "data": {'username': username, 'password' : password, 'dataHora' : dataHora, 'status': status, 'versao' : versao, 'uuid' : uuid, 'model' : model, 'versionDevice' : versionDevice}
			})
		};

		//função para Login por GET		
		/*var _doLogin = function(username, password) {
    		var login = url + 'login.php?username=' + username + '&password=' + password;

    		console.log(login);
    		return $http({
			   "url": login,
			   "method": "GET"
			});
    	};*/

    	// var _isLogged = function() {
	    //     // caso o local storage não esteja configurado, sai do sistema
	    //     return (localStorage.length > 0) ? true : false;
	    // };

	    //função para enviar (fazer INSERT) os dados de Check In do banco de dados do servidor na tabela cartao_ponto
    	var _doCheckIn = function(funcionario, checkin, cartao_pontocol, latitude, longitude, status, notacoes) {
    		return $http({
			   "url": url + "checkInOut_4724.php",
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'checkin': checkin, 'checkout': '', 'cartao_pontocol': cartao_pontocol, 'latitude': latitude, 'longitude': longitude, 'status' : status, 'notacoes' : notacoes}
			})
    	};
    	
    	//função para enviar (fazer INSERT) os dados de Check Out do banco de dados do servidor na tabela cartao_ponto
    	var _doCheckOut = function(funcionario, checkout, cartao_pontocol, latitude, longitude, status, notacoes) {
        	return $http({
			   "url":url + "checkInOut_4724.php",
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'checkin': '', 'checkout': checkout, 'cartao_pontocol': cartao_pontocol, 'latitude': latitude, 'longitude': longitude, 'status': status, 'notacoes' : notacoes}
			})
    	};

    	//função para enviar (fazer INSERT) das questões de Check List no banco de dados do servidor na tabela check_list
    	var _doCheckList = function(historyAnswers, funcionario, loja, dataHora, questao6Text, questao11Text, status) {
        	return $http({
			   "url":url + "checkList_415.php",
			   "method": "POST",
			   "data": {'history': historyAnswers, 'funcionario' : funcionario, 'loja': loja, 'dataHora': dataHora, 'questao6Text' : questao6Text, 'questao11Text' : questao11Text, 'status' : status}
			})
    	};

    	//função para enviar email informando o horário dde check in ou check out
    	var _sendEmail = function(data) {
        	return $http({
			   "url":url + "sendMail_415.php",
			   "method": "POST",
			   "data": data
			})
    	};

    	//função para receber as mensagens de Marketing
    	var _marketing = function(dataHora, funcionario) {
    		return $http({
			   "url": url + 'marketing_1412.php',
			   "method": "POST",
			   "data": {'dataHora': dataHora, 'funcionario' : funcionario}
			});
    	};

		//função para receber as rotas do funcionário
    	var _routes = function(funcionario, begin, end) {
    		return $http({
			   "url": url + 'routes_415.php',
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'begin' : begin, 'end' : end}
			});
    	};
	    
		//função para receber as entradas e saídas do funcionário (Dia atual)
    	var _report = function(value, begin, end, hasLoja) {
    		return $http({
			   "url": url + 'employee_415.php?value=' + value + '&begin=' + begin + '&end=' + end + '&hasLoja=' + hasLoja,
			   "method": "GET"
			});
    	};

		// //função para enviar a solicitação de materiais por email
		var _emailMateriais = function(data) {
    		return $http({
			   "url": url + 'materiais.php',
			   "method": "POST",
			   "data": data
			});
    	};

		//função para enviar a solicitação de uniformes por email
		var _emailUniformes = function(data) {
    		return $http({
			   "url": url + 'uniformes.php',
			   "method": "POST",
			   "data": data
			});
		};
		
		//função para enviar a solicitação de transferencia por email
		var _emailTransferencia = function(data) {
			return $http({
				"url": url + 'transferencia.php',
				"method": "POST",
				"data": data
			});
		};

		//função para receber questões do Check List
		var _receberCheckList = function(idChecklist) {
    		return $http({
			   "url": url + 'checklist_cadastro_415.php?action=GET&idChecklist=' + idChecklist,
			   "method": "GET"
			});
    	};

		//função para enviar respostas do Check List
		var _enviarCheckList = function(checklist, funcionario, dataHora, loja) {
			return $http({
			   "url": (url + 'checklist_cadastro_415.php?action=POST&idChecklist=' + checklist.checklist.id),
			   "method": "POST",
			   "data": { 'checklist': checklist, 'funcionario' : funcionario, 'dataHora': dataHora, 'loja': loja }
			})
		};
		
		// //função para desbloquear acesso de Login para colaboradores
		var _solicitarDesbloqueio = function(responsavel, username, dataHora) {
			return $http({
				"url": url + 'desbloqueio_415.php',
				"method": "POST",
				"data": { 'responsavel': responsavel, 'username' : username, 'dataHora': dataHora }
			})
		};

		//função para receber as entradas e saídas do funcionário (Espelho Colaborador)
		var _espelhoColaborador = function(value, begin, end, hasLoja) {
			return $http({
				"url": url + 'employee_415.php?value=' + value + '&begin=' + begin + '&end=' + end + '&hasLoja=' + hasLoja,
				"method": "GET"
			});
		};

		//função para receber sistema de permissões
		var _receberPermissoes = function(funcionario) {
			return $http({
				"url": url + 'getPermissoes.php?action=GET&funcionario=' + funcionario,
				"method": "GET"
			});
		};

		//função para enviar token FCM para servidor (POST)
		var _sendToken = function(funcionario, token) {
			return $http({
				"url": url + 'manageInstanceID.php',
				"method": "POST",
				"data": {'idUser': funcionario, 'tokenApp' : token}
			})
		};

		//função para receber todas as lojas
		var _receberLojas = function() {
			return $http({
				"url": url + 'getLojas.php',
				"method": "GET"
			});
		};

		//função para trocar senha do funcionário
		var _trocaSenha = function(funcionario, newPassword) {
			return $http({
				"url": url + 'trocaSenha.php',
				"method": "POST",
				"data": {'funcionario': funcionario, 'newPassword' : newPassword }
			})
		};

	    return {
	        doLogin: _doLogin,
	        // isLogged: _isLogged,
	       	doCheckIn: _doCheckIn,
	       	doCheckOut: _doCheckOut,
	       	doCheckList: _doCheckList,
	       	sendEmail: _sendEmail,
	       	marketing: _marketing,
			routes: _routes,
			report: _report,
			emailMateriais: _emailMateriais,
			emailUniformes: _emailUniformes,
			emailTransferencia: _emailTransferencia,
			receberCheckList: _receberCheckList,
			enviarCheckList: _enviarCheckList,
			solicitarDesbloqueio: _solicitarDesbloqueio,
			espelhoColaborador: _espelhoColaborador,
			receberPermissoes: _receberPermissoes,
			sendToken: _sendToken,
			receberLojas : _receberLojas,
			trocaSenha: _trocaSenha
	    };
	}

})();